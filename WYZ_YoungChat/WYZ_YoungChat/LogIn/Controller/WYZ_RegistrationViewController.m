//
//  WYZ_RegistrationViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_RegistrationViewController.h"

@interface WYZ_RegistrationViewController ()
@property(nonatomic, retain)UILabel *titleLabel;
@property(nonatomic, retain)UIView *backView;
@property(nonatomic, retain)UIImageView *phoneImage;
@property(nonatomic, retain)UIImageView *safeImage;
@property(nonatomic, retain)UIImageView *cipherImage;//密码
@property(nonatomic, retain)UIImageView *repeatCipherImage;//重复密码
@property(nonatomic, retain)UITextField *phoneTextF;
@property(nonatomic, retain)UITextField *safeTextF;
@property(nonatomic, retain)UITextField *cipherTextF;
@property(nonatomic, retain)UITextField *repeatCipherTextF;
@property(nonatomic, retain)UIView *backWireView;
@property(nonatomic, retain)UIButton *textButton;//验证按钮
@property(nonatomic, retain)UIButton *registerButton;//注册
@end

@implementation WYZ_RegistrationViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithWhite:0.929 alpha:1.000];
    
    //返回button
    UIImageView *returnImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 25, 30, 25)];
    returnImageView.image = [UIImage imageNamed:@"zuojiantou.png"];
    [self.view addSubview:returnImageView];
    
    
    UIButton *returnButton = [UIButton buttonWithType:UIButtonTypeSystem];
    returnButton.backgroundColor = [UIColor clearColor];
    returnButton.frame = CGRectMake(10, 25, 30, 25);
    [self.view addSubview:returnButton];
    [returnButton addTarget:self action:@selector(returnButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self createTitle];
    
    
}

#pragma mark 返回登录页面
-(void)returnButton:(UIButton *)but{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
}

-(void)createTitle{
    
    //创建标题用户注册
    self.titleLabel = [[UILabel alloc] init];
    [self.view addSubview:self.titleLabel];
    self.titleLabel.text = @"用户注册";
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30 * WIDTH];
    
    
    self.titleLabel.textColor = [UIColor orangeColor];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.mas_equalTo(140 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
        
    }];
    
    //创建背景底图白色
    self.backView = [[UIView alloc] init];
    self.backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(50);
        make.left.mas_equalTo(60 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(177 * HEIGHT);
        
    }];
    
    //创建手机图标
    self.phoneImage = [[UIImageView alloc] init];
    self.phoneImage.image = [UIImage imageNamed:@"phone.png"];
    [self.backView addSubview:self.phoneImage];
    [self.phoneImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(20 * WIDTH);
        make.height.mas_equalTo(22 * HEIGHT);
        
        
        
    }];
    
    
    
    
    //创建phoneTextF
    self.phoneTextF = [[UITextField alloc] init];
    [self.backView addSubview:self.phoneTextF];
    self.phoneTextF.placeholder = @"手机号";
    self.phoneTextF.font = [UIFont systemFontOfSize:17 * WIDTH];
    [self.phoneTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7);
        make.left.mas_equalTo(33 * WIDTH);
        make.width.mas_equalTo(160 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
        
    }];
    //验证按钮
    self.textButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.textButton.backgroundColor = [UIColor orangeColor];
    [self.textButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.textButton.titleLabel.font = [UIFont systemFontOfSize:16 * WIDTH];
    self.textButton.tintColor = [UIColor whiteColor];
    [self.backView addSubview:self.textButton];
    [self.textButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self.textButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(4);
        make.left.mas_equalTo(194 * WIDTH);
        make.width.mas_equalTo(102 *HEIGHT);
        
    }];
    
    //第一条背景线
    self.backWireView = [[UIView alloc] init];
    [self.backView addSubview:self.backWireView];
    self.backWireView.backgroundColor = [UIColor colorWithWhite:0.789 alpha:0.2 ];
    [self.backWireView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textButton.mas_bottom).offset(7);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(280 * WIDTH);
        make.height.mas_equalTo(1);
        
        
    }];
    
    //创建安全图标
    self.safeImage = [[UIImageView alloc] init];
    self.safeImage.image = [UIImage imageNamed:@"anquan.png"];
    [self.backView addSubview:self.safeImage];
    [self.safeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneTextF.mas_bottom).offset(14);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(20 * WIDTH);
        make.height.mas_equalTo(22 * HEIGHT);
        
        
    }];
    
    //创建safeTetF
    self.safeTextF = [[UITextField alloc] init];
    [self.backView addSubview:self.safeTextF];
    self.safeTextF.placeholder = @"验证码";
    self.safeTextF.font = [UIFont systemFontOfSize:17 * WIDTH];
    [self.safeTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneTextF.mas_bottom).offset(12);
        make.left.mas_equalTo(33 * WIDTH);
        make.width.mas_equalTo(280 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
        
    }];
    
    //第二条背景线
    self.backWireView = [[UIView alloc] init];
    [self.backView addSubview:self.backWireView];
    self.backWireView.backgroundColor = [UIColor colorWithWhite:0.917 alpha:1.000];
    [self.backWireView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTextF.mas_bottom).offset(7);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(280 * WIDTH);
        make.height.mas_equalTo(1);
        
    }];
    
    //创建密码图标
    self.cipherImage = [[UIImageView alloc] init];
    self.cipherImage.image = [UIImage imageNamed:@"mima.png"];
    [self.backView addSubview:self.cipherImage];
    [self.cipherImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTextF.mas_bottom).offset(14);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(20 * WIDTH);
        make.height.mas_equalTo(22 * HEIGHT);
        
        
    }];
    
    //创建密码
    self.cipherTextF = [[UITextField alloc] init];
    [self.backView addSubview:self.cipherTextF];
    self.cipherTextF.placeholder = @"密码";
    self.cipherTextF.secureTextEntry = YES;
    self.cipherTextF.clearButtonMode = UITextFieldViewModeAlways;
    self.cipherTextF.font = [UIFont systemFontOfSize:17 * WIDTH];
    [self.cipherTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTextF.mas_bottom).offset(12);
        make.left.mas_equalTo(33 * WIDTH);
        make.width.mas_equalTo(270 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
        
    }];
    
    //第三条背景线
    self.backWireView = [[UIView alloc] init];
    [self.backView addSubview:self.backWireView];
    self.backWireView.backgroundColor = [UIColor colorWithWhite:0.917 alpha:1.000];
    [self.backWireView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTextF.mas_bottom).offset(7);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(280 * WIDTH);
        make.height.mas_equalTo(1);
        
    }];
    
    //创建重复密码图标
    self.repeatCipherImage = [[UIImageView alloc] init];
    self.repeatCipherImage.image = [UIImage imageNamed:@"mima.png"];
    [self.backView addSubview:self.repeatCipherImage];
    [self.repeatCipherImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cipherTextF.mas_bottom).offset(14);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(20 * WIDTH);
        make.height.mas_equalTo(22 * HEIGHT);
        
        
    }];
    
    //重复创建密码
    self.repeatCipherTextF = [[UITextField alloc] init];
    [self.backView addSubview:self.repeatCipherTextF];
    self.repeatCipherTextF.placeholder = @"重复密码";
    self.repeatCipherTextF.secureTextEntry = YES;
    self.repeatCipherTextF.clearButtonMode = UITextFieldViewModeAlways;
    self.repeatCipherTextF.font = [UIFont systemFontOfSize:17 * WIDTH];
    [self.repeatCipherTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cipherTextF.mas_bottom).offset(12);
        make.left.mas_equalTo(33 * WIDTH);
        make.width.mas_equalTo(270 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
        
        
    }];
    
    //第四条背景线
    self.backWireView = [[UIView alloc] init];
    [self.backView addSubview:self.backWireView];
    self.backWireView.backgroundColor = [UIColor colorWithWhite:0.917 alpha:1.000];
    [self.backWireView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cipherTextF.mas_bottom).offset(7);
        make.left.mas_equalTo(10 * WIDTH);
        make.width.mas_equalTo(280 * WIDTH);
        make.height.mas_equalTo(1);
        
    }];
    
    //注册按钮
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerButton.backgroundColor = [UIColor orangeColor];
    [self.registerButton setTitle:@"注  册" forState:UIControlStateNormal];
    self.registerButton.titleLabel.font = [UIFont systemFontOfSize:19 * WIDTH];
    self.registerButton.tintColor = [UIColor whiteColor];
    [self.registerButton addTarget:self action:@selector(registerApp) forControlEvents:UIControlEventTouchUpInside];
    [self.registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backView.mas_bottom).offset(60);
        make.left.mas_equalTo(60 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(35 * WIDTH);
        [self.view addSubview:self.registerButton];
        
    }];
    
    
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    //注册键盘消失的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)name:UIKeyboardWillHideNotification object:nil];
    
    
}



//验证按钮
-(void)click{
    [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:self.phoneTextF.text zone:@"86" customIdentifier:nil result:^(NSError *error) {
        if (!error) {
            NSLog(@"获取验证码成功");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"验证码发送成功" message:@"恭喜!验证码发送成功" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
            [alert show];
            //点击事件延时
            self.textButton.userInteractionEnabled = NO;
            [self.textButton setTitle:@"15秒后再次发送" forState:UIControlStateNormal];
            self.textButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [self.textButton setTitleColor:[UIColor colorWithWhite:0.796 alpha:1.000] forState:UIControlStateNormal];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.textButton.userInteractionEnabled = YES;
                self.textButton.titleLabel.font = [UIFont systemFontOfSize:14];
                [self.textButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self.textButton setTitle:@"再次获取验证码" forState:UIControlStateNormal];
            });
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"验证码发送失败" message:@"手机号输入错误" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    
}


//注册按
-(void)registerApp{
    
    if (![self isEmpty]) {
        [SMSSDK commitVerificationCode:self.safeTextF.text phoneNumber:self.phoneTextF.text zone:@"86" result:^(NSError *error) {
            if (!error) {
                [[EaseMob sharedInstance].chatManager asyncRegisterNewAccount:self.phoneTextF.text password:self.cipherTextF.text withCompletion:^(NSString *username, NSString *password, EMError *error) {
                    if (!error) {
                        [self dismissViewControllerAnimated:YES completion:^{
                        }];
                    }else{
                        //手机号已被注册
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"注册失败" message:@"手机号已被注册" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                } onQueue:nil];
            }
            else
            {
                //验证码输入错误
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"注册失败" message:@"验证码输入错误" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"注册失败" message:@"验证码为空或两次输入密码不同" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    
}


#pragma mark 判断用户名密码是否为空,密码是否相等
- (BOOL)isEmpty{
    BOOL ret = NO;
    NSString *username = self.phoneTextF.text;
    NSString *password = self.cipherTextF.text;
    if (username.length == 0 || password.length == 0 || ![self.cipherTextF.text isEqualToString:self.repeatCipherTextF.text]) {
        ret = YES;
    }
    return ret;
}

#pragma mark 点击空白处回收键盘
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    //点击空白处 回收键盘
    [self.phoneTextF resignFirstResponder];
    [self.cipherTextF resignFirstResponder];
    [self.safeTextF resignFirstResponder];
    [self.repeatCipherTextF resignFirstResponder];
    [UIView animateWithDuration:2 animations:^{
        
    }];
}
#pragma mark 键盘弹起的时候触发的方法
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    
    
}
#pragma mark 监听键盘回收的时候触发的方法
-(void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    
    
    
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
