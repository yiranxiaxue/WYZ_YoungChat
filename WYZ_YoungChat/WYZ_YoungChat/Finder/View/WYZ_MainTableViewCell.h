//
//  WYZ_MainTableViewCell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_MainTableViewCell : WYZ_BaseTableViewCell
@property (nonatomic,strong) UILabel *titleName;

@property (nonatomic,strong) UIImageView *picImageView;

@property (nonatomic,strong) UIImageView *pushImageView;

@property (nonatomic,strong) UILabel * smallTitleName;
@end
