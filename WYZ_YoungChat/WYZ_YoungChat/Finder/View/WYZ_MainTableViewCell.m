//
//  WYZ_MainTableViewCell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_MainTableViewCell.h"

@implementation WYZ_MainTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self createView];
    }
    return self;
}

-(void)createView{
    self.titleName = [[UILabel alloc] init];
    [self.contentView addSubview:_titleName];
    _titleName.font = [UIFont systemFontOfSize:15];
    _titleName.tintColor = [UIColor orangeColor];
    
    self.picImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:_picImageView];
    
    self.pushImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:_pushImageView];
    
    self.smallTitleName = [[UILabel alloc] init];
    [self.contentView addSubview:_smallTitleName];

}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.bottom.mas_equalTo(-5);
        make.top.mas_equalTo(5);
        make.height.mas_equalTo(@[_titleName,_smallTitleName]);
        make.width.mas_equalTo(25);
    }];
    [_titleName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_picImageView.mas_right).offset(10);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(80);
    }];
    
    [_smallTitleName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_titleName.mas_right).offset(100);
        make.top.mas_equalTo(5);
        make.right.mas_equalTo(_pushImageView.mas_left).offset(-10);
    }];
    
    [_pushImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(20);
    }];
    
    
    
    
}


@end
