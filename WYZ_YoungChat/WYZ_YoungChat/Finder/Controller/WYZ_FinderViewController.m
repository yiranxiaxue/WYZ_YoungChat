//
//  WYZ_FinderViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_FinderViewController.h"

#import "WYZ_MainTableViewCell.h"

#import "WYZ_MapViewController.h"

#import "LWS_ClassifyViewController.h"

#import "WYZ_CrosstalkViewController.h"

#import "WYZ_WritingViewController.h"

#import <objc/runtime.h>

#import <QuartzCore/QuartzCore.h>

#import <Accelerate/Accelerate.h>

#import "DropView.h"

#import "Header.h"

#import "WYZ_LogInViewController.h"


#define  Kscreen_Wight  self.view.frame.size.width
#define  Kscreen_Height self.view.frame.size.height
@interface WYZ_FinderViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITableView *mainTableView;

@property (nonatomic,strong)UIImageView *pictureImageView;
@property (nonatomic,retain)DropView *drop;


@property (nonatomic,strong) UIImageView *photoImageView;

@property (nonatomic,strong) UILabel *userNameLabel;


@end


@implementation WYZ_FinderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    
    UILabel *laber =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    
    laber.text = @"seek novelty";
    
    laber.font =[UIFont fontWithName:@"Zapfino" size:20*WIDTH];
    
    self.navigationItem.titleView = laber;
    
    
    [self createView];
}
-(void)createView
{
 
    
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    

   
    _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,  HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _mainTableView.contentInset = UIEdgeInsetsMake(HeightImage, 0, 0, 0);
    [self.view addSubview:self.mainTableView];
   
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    
    _drop = [[DropView alloc] initWithFrame:CGRectMake(0, -HeightImage, Kscreen_Wight, HeightImage)];
    _drop.contentMode = UIViewContentModeScaleAspectFill;
    _drop.imageName = @"car@2x.jpg";
    [_mainTableView addSubview:_drop];
    
       _mainTableView.backgroundColor =  [UIColor colorWithRed:0.944 green:0.885 blue:0.882 alpha:1.000];
    _drop.autoresizesSubviews = YES;
    [self.view addSubview:_mainTableView];
    
     [_mainTableView registerClass:[WYZ_MainTableViewCell class] forCellReuseIdentifier:@"WYZ_MainTableViewCell"];
    
    
    UIButton *outLog = [UIButton  buttonWithType:UIButtonTypeSystem];
    outLog.frame = CGRectMake(290 * WIDTH, 20 * HEIGHT, 100 * WIDTH, 30 * HEIGHT);
    outLog.backgroundColor = [UIColor colorWithWhite:0.247 alpha:0.332];
    [outLog setTitle:@"退出登录" forState:UIControlStateNormal];
    [outLog setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [outLog addTarget:self action:@selector(outLog:) forControlEvents:UIControlEventTouchUpInside];
    [_drop addSubview:outLog];
    

}


-(void)outLog:(UIButton *)but{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
 
        if (error && error.errorCode != EMErrorServerNotLogin) {
          
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
        }
    } onQueue:nil];

 
}

//偏移量
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGFloat y = scrollView.contentOffset.y ;
    if (y<-HeightImage) {
        _drop.height= -y;
    }


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 != 0) {
        return 20;
    }else{
        return 40;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WYZ_MainTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor colorWithRed:0.944 green:0.885 blue:0.882 alpha:1.000];
    if (indexPath.row == 0) {
        cell.titleName.text = @"选礼神器";
        
        cell.picImageView.image = [UIImage imageNamed:@"liwu.png"];
        cell.pushImageView.image = [UIImage imageNamed:@"trewq.png"];
        cell.backgroundColor = [UIColor whiteColor];
    }else if (indexPath.row == 2){
        cell.titleName.text = @"聆听电台";
        cell.picImageView.image = [UIImage imageNamed:@"diantai.png"];
        cell.pushImageView.image = [UIImage imageNamed:@"trewq.png"];
        cell.backgroundColor = [UIColor whiteColor];
        
    }else if (indexPath.row == 4){
        cell.titleName.text = @"地图导航";
        cell.picImageView.image = [UIImage imageNamed:@"ditu.png"];
        cell.pushImageView.image = [UIImage imageNamed:@"trewq.png"];
        cell.backgroundColor = [UIColor whiteColor];
    }else if (indexPath.row == 6){
        cell.titleName.text = @"欢快划线";
        cell.picImageView.image = [UIImage imageNamed:@"huashishouxieshelleyscriptltstd.png"];
        cell.pushImageView.image = [UIImage imageNamed:@"trewq.png"];
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 4) {
        self.hidesBottomBarWhenPushed = YES;
        WYZ_MapViewController *mapVC = [[WYZ_MapViewController alloc] init];
        [self.navigationController pushViewController:mapVC animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }else if (indexPath.row == 0){
    
        self.hidesBottomBarWhenPushed = YES;
        LWS_ClassifyViewController *lwsVC = [[LWS_ClassifyViewController alloc]initWithNibName:NSStringFromClass([LWS_ClassifyViewController class]) bundle:nil];
        [self.navigationController pushViewController:lwsVC animated:YES];
        self.hidesBottomBarWhenPushed = NO;
      
    
    }else if (indexPath.row == 2){
        WYZ_CrosstalkViewController *crossVC = [[WYZ_CrosstalkViewController alloc]init];
        [self.navigationController pushViewController:crossVC animated:YES];
    
    }else if (indexPath.row == 6){
    self.hidesBottomBarWhenPushed = YES;
        WYZ_WritingViewController *writingVC = [[WYZ_WritingViewController alloc] init];
        [self.navigationController pushViewController:writingVC animated:YES];
    self.hidesBottomBarWhenPushed = NO;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
