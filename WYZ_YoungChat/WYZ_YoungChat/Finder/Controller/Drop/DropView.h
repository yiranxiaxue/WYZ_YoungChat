//
//  DropView.h
//  DropDownDemo
//
//  Created by MengFanBiao on 15/12/4.
//  Copyright © 2015年 MengFanBiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropView : UIView

@property (nonatomic,copy) NSString *imageName;
@property (nonatomic,assign) CGFloat height;

- (void)setImageName:(NSString *)imageName;

- (void)setHeight:(CGFloat)height;
@end
