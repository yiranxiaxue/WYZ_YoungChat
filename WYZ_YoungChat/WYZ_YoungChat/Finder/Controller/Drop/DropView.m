//
//  DropView.m
//  DropDownDemo
//
//  Created by MengFanBiao on 15/12/4.
//  Copyright © 2015年 MengFanBiao. All rights reserved.
//

#import "DropView.h"
#import "Header.h"
@interface DropView()
{
    UIImageView *imageview;
    UIVisualEffectView *effectview;
}
@end

@implementation DropView

- (id) initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleAspectFill;

        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
        
        effectview.frame = CGRectMake(0, 0, self.frame.size.width, self
                                      .frame.size.height);
        effectview.alpha= 0;
        
        [imageview addSubview:effectview];

        [self addSubview:imageview];
    }
    
    return self;
}
-(void)setImageName:(NSString *)imageName{
    imageview.image = [UIImage imageNamed:imageName];
}
-(void)setHeight:(CGFloat)height{
    self.frame=CGRectMake(0, -height, self.frame.size.width, height);
    imageview.frame=CGRectMake(0, 0, self.frame.size.width, height);
    effectview.frame=CGRectMake(0, 0, self.frame.size.width, height);
    effectview.alpha = (height-400)/(Kscreen_Height-250-100);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
