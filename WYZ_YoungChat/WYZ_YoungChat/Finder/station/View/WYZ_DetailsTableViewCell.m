//
//  WYZ_DetailsTableViewCell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_DetailsTableViewCell.h"

@implementation WYZ_DetailsTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelView];
    }
    
    return self;
}
-(void)creatTabelView
{
    //左图片
    self.leftImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.leftImageView];
    self.leftImageView.layer.cornerRadius =25*WIDTH;
    self.leftImageView.layer.masksToBounds = YES;
    
    //播放图标
    self.playImageView  =[[UIImageView alloc] init];
    [self.contentView addSubview:self.playImageView];
    self.playImageView.image =[UIImage imageNamed:@"hjll.png"];
    
    //评论图标
    self.commentImageView  =[[UIImageView alloc] init];
    [self.contentView addSubview:self.commentImageView];
    self.commentImageView.image = [UIImage imageNamed:@"aaaa.png"];
    
    //名字
    self.nameLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.nameLabel];
    self.nameLabel.font =[UIFont systemFontOfSize:15*WIDTH];
    
    //播放人数
    self.playPeopleLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.playPeopleLabel];
    self.playPeopleLabel.font =[UIFont systemFontOfSize:15*WIDTH];
    
    //评论人数
    self.commentPeopleLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.commentPeopleLabel];
    self.commentPeopleLabel.textAlignment =1;
    self.commentPeopleLabel.font =[UIFont systemFontOfSize:15*WIDTH];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    //左图片
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(10*WIDTH);
        make.width.mas_equalTo(50*WIDTH);
        make.height.mas_equalTo(50*HEIGHT);
    }];
    
    //名字
    self.nameLabel.numberOfLines =0;
    [self.nameLabel sizeToFit];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(300*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    //播放图片
    [self.playImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    //播放人数
    [self.playPeopleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.playImageView.mas_right).offset(0);
        make.width.mas_equalTo(70*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    //评论图标
    [self.commentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.playPeopleLabel.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    //评论人数
    [self.commentPeopleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.commentImageView.mas_right).offset(0);
        make.width.mas_equalTo(40*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
}


@end
