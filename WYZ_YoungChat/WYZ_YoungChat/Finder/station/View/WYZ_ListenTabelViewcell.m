//
//  WYZ_ListenTabelViewcell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_ListenTabelViewcell.h"

@implementation WYZ_ListenTabelViewcell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelViewCell];
    }
    return  self;
}
-(void)creatTabelViewCell
{
    //左图
    self.leftImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.leftImageView];
    
    //集数图标
    self.downImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.downImageView];
    self.downImageView.image =[UIImage imageNamed:@"jiji.png"];
    
    //右图标
    self.rightImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.rightImageView];
    self.rightImageView.image =[UIImage imageNamed:@"yuio.png"];
    
    //上label
    self.topLabel =[[UILabel alloc] init];
    self.topLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.contentView addSubview:self.topLabel];
    
    //中间label
    self.sencerLabel =[[UILabel alloc] init];
    self.sencerLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.contentView addSubview:self.sencerLabel];
    
    //下label
    self.downLabel =[[UILabel alloc] init];
    self.downLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.contentView addSubview:self.downLabel];
    
    //numbelLabel
    self.numberLabel =[[UILabel alloc] init];
    self.numberLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.contentView addSubview:self.numberLabel];
    self.numberLabel.textAlignment =1;
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    ///number
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(30*WIDTH);
        make.height.mas_equalTo(CELLHEIGHT);
    }];
    ///左图
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(self.numberLabel.mas_right).offset(0);
        make.width.mas_equalTo(80*WIDTH);
        make.height.mas_equalTo(CELLHEIGHT-20*HEIGHT);
    }];
    
    ///上label
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(200*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    
    ///中间label
    [self.sencerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(250*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    ///集数图标
    [self.downImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sencerLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
    ///下label
    [self.downLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sencerLabel.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.downImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(100*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
    ///右图标
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLabel.mas_bottom).offset(10*HEIGHT);
        make.right.mas_equalTo(-10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
    
    
    
}


@end
