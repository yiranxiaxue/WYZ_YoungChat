//
//  WYZ_TOPtabelViewcell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_TOPtabelViewcell : WYZ_BaseTableViewCell
@property(nonatomic,retain)UIImageView  *leftImageView; //左图
@property(nonatomic,retain)UILabel *topLabel;//上label
@property(nonatomic,retain)UILabel  *downLabel;//下label
@property(nonatomic,retain)UIImageView *rightImageView;//右图片
@end
