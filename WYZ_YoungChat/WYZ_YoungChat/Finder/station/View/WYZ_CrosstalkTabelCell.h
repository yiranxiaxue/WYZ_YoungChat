//
//  WYZ_CrosstalkTabelCell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_CrosstalkTabelCell : WYZ_BaseTableViewCell
@property(nonatomic,retain)UIImageView  *leftImageView;//左图
@property(nonatomic,retain)UIImageView *rightImageView;//右图标
@property(nonatomic,retain)UIImageView *downImageView;//播放次数图标
@property(nonatomic,retain)UIImageView *rightDownImageView;//播放集数图标
@property(nonatomic,retain)UILabel  *topLabel;//上label
@property(nonatomic,retain)UILabel *centreLabel;//中间label
@property(nonatomic,retain)UILabel *playsLabel;//播放次数
@property(nonatomic,retain)UILabel *PlayBluesLabel;//播放集数
@end
