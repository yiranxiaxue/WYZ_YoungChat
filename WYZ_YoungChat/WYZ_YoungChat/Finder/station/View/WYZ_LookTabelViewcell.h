//
//  WYZ_LookTabelViewcell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_LookTabelViewcell : WYZ_BaseTableViewCell
@property(nonatomic,retain)UIImageView *picimageView;
@property(nonatomic,retain)UILabel *leftLabel;
@property(nonatomic,retain)UILabel *rightLabel;
@end
