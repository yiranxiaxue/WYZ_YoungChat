//
//  WYZ_DetailsTableViewCell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_DetailsTableViewCell : WYZ_BaseTableViewCell
@property(nonatomic,retain)UIImageView *leftImageView;//左图片
@property(nonatomic,retain)UIImageView *playImageView;//播放图标
@property(nonatomic,retain)UIImageView *commentImageView;//评论图标
@property(nonatomic,retain)UILabel *nameLabel;//名字
@property(nonatomic,retain)UILabel *playPeopleLabel;//播放人数
@property(nonatomic,retain)UILabel *commentPeopleLabel;//评论人数
@end
