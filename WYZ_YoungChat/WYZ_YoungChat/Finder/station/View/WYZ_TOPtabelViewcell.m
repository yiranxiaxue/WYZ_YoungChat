//
//  WYZ_TOPtabelViewcell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_TOPtabelViewcell.h"

@implementation WYZ_TOPtabelViewcell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelViewCell];
    }
    return  self;
}
-(void)creatTabelViewCell
{
    //左图
    self.leftImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.leftImageView];
    //上label
    self.topLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.topLabel];
    self.topLabel.font = [UIFont systemFontOfSize:17*WIDTH];
    //下label
    self.downLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.downLabel];
    
    self.downLabel.font = [UIFont systemFontOfSize:17*WIDTH];
    
    //右图片
    self.rightImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.rightImageView];
    self.rightImageView.image =[UIImage imageNamed:@"yuio.png"];
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    //左图
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(10*WIDTH);
        make.width.mas_equalTo(80*WIDTH);
        make.height.mas_equalTo(CELLHEIGHT-20*HEIGHT);
    }];
    
    //上label
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(200*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    
    //下label
    [self.downLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(250*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    
    //右图片
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30*HEIGHT);
        make.right.mas_equalTo(-10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
}


@end
