//
//  WYZ_LookforCollectionCell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_LookforCollectionCell.h"

@implementation WYZ_LookforCollectionCell


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self collectionCell];
    }
    return self;
}

-(void)collectionCell
{
    self.titleLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.textAlignment= 1;
    self.titleLabel.layer.borderWidth = 1;
    self.titleLabel.layer.cornerRadius =15*WIDTH;
    self.titleLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    self.titleLabel.layer.borderColor=[[UIColor grayColor]CGColor];
    self.titleLabel.layer.masksToBounds = YES;
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo( CELLWIDTH);
        make.height.mas_equalTo(25*WIDTH);
    }];
}


@end
