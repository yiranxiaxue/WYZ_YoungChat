//
//  WYZ_HisCollectionViewcell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_HisCollectionViewcell.h"

@implementation WYZ_HisCollectionViewcell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self collectionCell];
    }
    return self;
}

-(void)collectionCell
{
    self.titLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.titLabel];
    self.titLabel.textAlignment= 1;
    //    self.titLabel.layer.borderWidth= 1;
    self.titLabel.layer.cornerRadius =15*WIDTH;
    self.titLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    self.titLabel.layer.borderColor=[[UIColor grayColor]CGColor];
    self.titLabel.layer.masksToBounds = YES;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.titLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo( CELLWIDTH);
        make.height.mas_equalTo(25*WIDTH);
    }];
}


@end
