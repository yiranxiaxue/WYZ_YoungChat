//
//  WYZ_ListenTabelViewcell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_ListenTabelViewcell : WYZ_BaseTableViewCell
@property(nonatomic,retain)UIImageView *leftImageView;//左图
@property(nonatomic,retain)UIImageView  *downImageView;//集数图标
@property(nonatomic,retain)UIImageView *rightImageView;//右图标
@property(nonatomic,retain)UILabel  *topLabel;//上label
@property(nonatomic,retain)UILabel  *sencerLabel;//中间label
@property(nonatomic,retain)UILabel  *downLabel;//下label
@property(nonatomic,retain)UILabel  *numberLabel; //排序
@end
