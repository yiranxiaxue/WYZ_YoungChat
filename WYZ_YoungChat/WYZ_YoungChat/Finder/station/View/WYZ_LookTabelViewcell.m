//
//  WYZ_LookTabelViewcell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_LookTabelViewcell.h"

@implementation WYZ_LookTabelViewcell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelView];
    }
    return self;
}
-(void)creatTabelView
{
    //图片
    self.picimageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.picimageView];
    
    //名字
    self.leftLabel = [[UILabel alloc] init];
    self.leftLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    self.leftLabel.font = [UIFont systemFontOfSize:15*WIDTH];
    [self.contentView addSubview:self.leftLabel];
    
    //有声书
    self.rightLabel = [[UILabel alloc] init];
    self.rightLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    self.rightLabel.font = [UIFont systemFontOfSize:15*WIDTH];
    [self.contentView addSubview:self.rightLabel];
    
    
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.picimageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5*WIDTH);
        make.top.mas_equalTo(5*HEIGHT);
        make.width.mas_equalTo(50*WIDTH);
        make.height.mas_equalTo(40*HEIGHT);
    }];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.picimageView.mas_right).offset(10*WIDTH);
        make.top.mas_equalTo(5*HEIGHT);
        make.width.mas_equalTo(200*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftLabel.mas_right).offset(10*WIDTH);
        make.top.mas_equalTo(10*HEIGHT);
        make.width.mas_equalTo(70*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
}



@end
