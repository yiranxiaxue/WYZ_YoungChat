//
//  WYZ_LookforCollectionCell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseCollectionViewCell.h"

@interface WYZ_LookforCollectionCell : WYZ_BaseCollectionViewCell
@property(nonatomic,retain)UILabel *titleLabel;
@end
