//
//  WYZ_CrosstalkTabelCell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_CrosstalkTabelCell.h"

@implementation WYZ_CrosstalkTabelCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelViewCell];
    }
    return  self;
}
-(void)creatTabelViewCell
{
    //左图
    self.leftImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.leftImageView];
    
    //右图
    self.rightImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.rightImageView];
    self.rightImageView.image =[UIImage imageNamed:@"yuio.png"];
    
    
    
    //播放次数图标
    self.downImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.downImageView];
    self.downImageView.image =[UIImage imageNamed:@"hjll.png"];
    
    
    //播放集数图标
    self.rightDownImageView =[[UIImageView alloc] init];
    [self.contentView addSubview:self.rightDownImageView];
    self.rightDownImageView.image =[UIImage imageNamed:@"jiji.png"];
    
    
    //上label
    self.topLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.topLabel];
    self.topLabel.font = [UIFont systemFontOfSize:20*WIDTH];
    self.topLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    
    //中间label
    self.centreLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.centreLabel];
    self.centreLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    
    //播放次数
    self.playsLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.playsLabel];
    self.playsLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    //播放集数
    self.PlayBluesLabel =[[UILabel alloc] init];
    [self.contentView addSubview:self.PlayBluesLabel];
    self.PlayBluesLabel.font =[UIFont systemFontOfSize:17*WIDTH];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    ///左图
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(10*WIDTH);
        make.width.mas_equalTo(80*WIDTH);
        make.height.mas_equalTo(CELLHEIGHT-20*HEIGHT);
    }];
    ///右图标
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43*HEIGHT);
        make.right.mas_equalTo(-5*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    ///上label
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(200*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    ///中间label
    [self.centreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(250*WIDTH);
        make.height.mas_equalTo(35*HEIGHT);
    }];
    ///播放次数图标
    //    self.downImageView.frame = CGRectMake(100*WIDTH, 70*HEIGHT, 20*WIDTH, 20*HEIGHT);
    [self.downImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.centreLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    ///播放次数
    [self.playsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.centreLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.downImageView.mas_right).offset(0);
        make.width.mas_equalTo(80*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    ///播放集数图标
    //    self.rightDownImageView.frame = CGRectMake(200*WIDTH,70*HEIGHT,20*WIDTH,20*HEIGHT);
    
    [self.rightDownImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.centreLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.playsLabel.mas_right).offset(0);
        make.width.mas_equalTo(20*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
    ///播放集数
    //    self.PlayBluesLabel.frame = CGRectMake(220*WIDTH, 70*HEIGHT, 80*WIDTH, 20*HEIGHT);
    [self.PlayBluesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.centreLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.rightDownImageView.mas_right).offset(0);
        make.width.mas_equalTo(80*WIDTH);
        make.height.mas_equalTo(20*HEIGHT);
    }];
    
    
}


@end
