//
//  WYZ_TOPViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_TOPViewController.h"
#import "WYZ_TOPModel.h"
#import "WYZ_ListenTabelViewcell.h"
#import "WYZ_DetailsViewController.h"



#define  NIUBI @"http://mobile.ximalaya.com/mobile/discovery/v1/rankingList/album?device=iPhone&key=ranking%3Aalbum%3Aplayed%3A7%3A12&pageId=1&pageSize=20&position=&title=%E7%9B%B8%E5%A3%B0%E8%AF%84%E4%B9%A6%E6%8E%92%E8%A1%8C%E6%A6%9C"
#define  LOADING @"http://mobile.ximalaya.com/mobile/discovery/v1/rankingList/album?device=iPhone&key=ranking%%3Aalbum%%3Aplayed%%3A7%%3A12&pageId=%ld&pageSize=20&position=&title=%%E7%%9B%%B8%%E5%%A3%%B0%%E8%%AF%%84%%E4%%B9%%A6%%E6%%8E%%92%%E8%%A1%%8C%%E6%%A6%%9C"

@interface WYZ_TOPViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,retain)MBProgressHUD *HUD;//小菊花
@property(nonatomic,retain)UITableView *tabelView;//tabelView
@property(nonatomic,retain)NSMutableArray *tabelTabelArr;
@property(nonatomic,assign)NSInteger  page;
@end

@implementation WYZ_TOPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.page = 2;
    [self creatHUD];//小菊花
    [self creatPilot];//导航栏设置
    [self createParsing];//数据解析
    [self creatTabelView];//tabelView
}
#pragma mark 数据解析
-(void)createParsing
{
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    [manager GET:NIUBI parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        self.tabelTabelArr =[NSMutableArray array];
        for (NSDictionary *tabelDic in responseObject[@"list"])
        {
            WYZ_TOPModel *mod =[[WYZ_TOPModel alloc] init];
            [mod setValuesForKeysWithDictionary:tabelDic];
            [self.tabelTabelArr addObject:mod];
        }
        
        [self.tabelView reloadData];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        //           NSLog(@"%@",error);
        self.HUD.hidden = YES;
    }];
    
}

#pragma mark tabelView
-(void)creatTabelView
{
    self.tabelView =[[UITableView alloc] init];
    [self.view addSubview:self.tabelView];
    [self.tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    self.tabelView.rowHeight =100*HEIGHT;
    self.tabelView.dataSource =self;
    self.tabelView.delegate  =self;
    [self.tabelView registerClass:[WYZ_ListenTabelViewcell class] forCellReuseIdentifier:@"reuse"];
    
    self.tabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    
    self.tabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
}
#pragma mark  图片下部上拉加载
-(void)footerRereshing
{
    
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:LOADING,self.page] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        for (NSDictionary *tabelDic in responseObject[@"list"])
        {
            WYZ_TOPModel *mod =[[WYZ_TOPModel alloc] init];
            [mod setValuesForKeysWithDictionary:tabelDic];
            [self.tabelTabelArr addObject:mod];
        }
        [self.tabelView reloadData];
        [self.tabelView.mj_header endRefreshing];
        [self.tabelView.mj_footer endRefreshing];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        self.HUD.hidden = YES;
    }];
    self.page++;
}
#pragma mark 上部下拉刷新
-(void)headerRereshing
{
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    [manager GET:NIUBI parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        self.tabelTabelArr =[NSMutableArray array];
        for (NSDictionary *tabelDic in responseObject[@"list"])
        {
            WYZ_TOPModel *mod =[[WYZ_TOPModel alloc] init];
            [mod setValuesForKeysWithDictionary:tabelDic];
            [self.tabelTabelArr addObject:mod];
        }
        
        [self.tabelView reloadData];
        [self.tabelView.mj_header endRefreshing];
        [self.tabelView.mj_footer endRefreshing];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        //           NSLog(@"%@",error);
        self.HUD.hidden = YES;
    }];
    
    
}
#pragma mark  cell点击方法
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_TOPModel *mod =self.tabelTabelArr[indexPath.row];
    WYZ_DetailsViewController *dvc =[[WYZ_DetailsViewController alloc] init];
    dvc.albumId = mod.albumId;
    dvc.name = mod.title;
    [self.navigationController pushViewController:dvc animated:YES];
    NSLog(@"%@---%@",mod.albumId,mod.title);
    
}
#pragma mark  几个cell
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tabelTabelArr.count;
}
#pragma mark  创建cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_TOPModel *mod = self.tabelTabelArr[indexPath.row];
    WYZ_ListenTabelViewcell *cell =[tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    [cell.leftImageView setImageWithURL:[NSURL URLWithString:mod.coverMiddle]];
    cell.topLabel.text = mod.title;
    cell.sencerLabel.text =mod.intro;
    cell.downLabel.text =[NSString stringWithFormat:@"%@集",mod.tracksCounts];
    cell.numberLabel.text =[NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    return cell;
}
#pragma mark  导航栏设置
-(void)creatPilot
{
    
    self.view.backgroundColor =[UIColor whiteColor];
    UILabel *label =[[UILabel alloc] initWithFrame:CGRectMake(64, 0, 70, 50)];
    label.text = @"相声评书排行榜";
    self.navigationItem.titleView =label;
        self.navigationController.navigationBar.barTintColor  = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"qwe.png"] style:1 target:self action:@selector(barButton:)];
}
#pragma mark button  点击方法
-(void)barButton:(UIButton *)button
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 小菊花
-(void)creatHUD
{
    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.HUD.labelText =@"正在努力加载中,请稍候";
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
