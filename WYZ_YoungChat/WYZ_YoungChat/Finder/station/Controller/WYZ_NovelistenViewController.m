//
//  WYZ_NovelistenViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_NovelistenViewController.h"
#import <AVFoundation/AVFoundation.h>
@interface WYZ_NovelistenViewController ()
@property(nonatomic,retain)UIImageView *photoImageView;//头像
@property(nonatomic,retain)UIImageView *bigImageView;//大图
@property(nonatomic,retain)UILabel *nameLabel;//名字
@property(nonatomic,retain)UISlider *slider;//左右滑动
@property(nonatomic,retain)UIButton *playButton;//开始按钮
@property(nonatomic,retain)UIView *downView;//底view
@property(nonatomic,retain)UIImageView *imageView;

//****  播放音频   ⬇️ ****///
@property (nonatomic, assign)CGFloat duration;
@property(nonatomic,retain)AVPlayer *player;//播放音频
@property(nonatomic,retain)AVPlayerItem  *playerItem;
@property (nonatomic, assign)BOOL isStart;//bool
@property(nonatomic,retain)UILabel *timeLabel;
//****  播放音频   ⬆️ ****///

@end

@implementation WYZ_NovelistenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    [self creatPilot];
    [self creatOther]; //其它控件
    [self addProgress];//对播放进度进行设置
    
}
#pragma mark其它控件
-(void)creatOther
{
        self.navigationController.navigationBar.barTintColor  = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    self.view.backgroundColor=[UIColor colorWithRed:0.901 green:0.904 blue:0.850 alpha:1.000];
    
    self.imageView =[[UIImageView alloc] init];
    [self.view  addSubview:self.imageView];
    [self.imageView setImageWithURL:[NSURL URLWithString:self.coverLarge]];
    self.imageView.alpha = 0.5;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(SCREEN_HEIGHT-49*HEIGHT);
    }];
    
    
    self.downView =[[UIView alloc] init];
    [self.view addSubview:self.downView];
    [self.downView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(SCREEN_HEIGHT);
    }];
    
    ///头像
    self.photoImageView =[[UIImageView alloc] init];
    [self.downView addSubview:self.photoImageView];
    self.photoImageView.layer.cornerRadius =40*WIDTH;
    self.photoImageView.layer.masksToBounds =YES;
    [self.photoImageView setImageWithURL:[NSURL URLWithString:self.smallLogo]];
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(10*WIDTH);
        make.width.mas_equalTo( 80*WIDTH);
        make.height.mas_equalTo(80*WIDTH);
    }];
    
    
    ///名字
    self.nameLabel =[[UILabel alloc] init];
    self.nameLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.downView addSubview:self.nameLabel];
    self.nameLabel.text = self.nickname;
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30*HEIGHT);
        make.left.mas_equalTo(self.photoImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo( 200*WIDTH);
        make.height.mas_equalTo(40*WIDTH);
    }];
    
    ///大图
    self.bigImageView =[[UIImageView alloc] init];
    self.bigImageView.backgroundColor = [UIColor colorWithRed:0.901 green:0.904 blue:0.850 alpha:1.000];
    [self.downView addSubview:self.bigImageView];
    self.bigImageView.layer.cornerRadius = 160*WIDTH;
    self.bigImageView.layer.masksToBounds =YES;
    [self.bigImageView setImageWithURL:[NSURL URLWithString:self.coverLarge]];  
    [self.bigImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(30*HEIGHT);
        make.left.mas_equalTo(50*WIDTH);
        make.width.mas_equalTo( 320*WIDTH);
        make.height.mas_equalTo(320*WIDTH);
    }];
    
    
    [self  creatTT];
    
    
    ///左右滑动按钮
    self.slider =[[UISlider alloc] init];
    [self.downView addSubview:self.slider];
    self.slider.tintColor =[UIColor redColor];
    self.slider.minimumTrackTintColor = [UIColor orangeColor];//左条颜色
    self.slider.maximumTrackTintColor = [UIColor whiteColor]; //右条颜色
    [self.slider addTarget:self action:@selector(slider:) forControlEvents:UIControlEventValueChanged];
    [self.slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bigImageView.mas_bottom).offset(20*HEIGHT);
        make.left.mas_equalTo(50*WIDTH);
        make.width.mas_equalTo(SCREEN_WIDTH-100*WIDTH);
        make.height.mas_equalTo(50*HEIGHT);
    }];
    
    ///开始按钮
    self.playButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [self.downView addSubview:self.playButton];
    self.playButton.layer.cornerRadius = 25*WIDTH;
    self.playButton.layer.masksToBounds = YES;
    [self.playButton setImage:[UIImage imageNamed:@"ting.png"] forState:UIControlStateNormal];
    [self.playButton addTarget:self action:@selector(butotn:) forControlEvents:UIControlEventTouchUpInside];
    [self.playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.slider.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(SCREEN_WIDTH/2-30*WIDTH);
        make.width.mas_equalTo( 50*WIDTH);
        make.height.mas_equalTo(50*WIDTH);
    }];
    
    
    
    
    //*************    播放音频
    NSString *urlStr = self.playUrl64;
    //     对网址进行转码
    NSLog(@"---------------%@",self.playUrl64);
    urlStr =[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlStr];
    //     根据网址,创建视频项目对象
    self.playerItem = [[AVPlayerItem alloc] initWithURL:url];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    [self.player play];
    
    ///时间label
    self.timeLabel =[[UILabel alloc] init];
    self.timeLabel.text =@"00:00/00:00";
    self.timeLabel.font = [UIFont systemFontOfSize:17*WIDTH];
    [self.view addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.slider.mas_bottom).offset(10*HEIGHT);
        make.left.mas_equalTo(self.playButton.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo( 100*WIDTH);
        make.height.mas_equalTo(50*WIDTH);
    }];
}
-(void)creatTT
{
    //layer 动画
    //控件可以拆分成两部分,一部分是控件的样式,比如边框,文字等这些都是通过操作控件层来完成的,另一部分是控件的功能,比如button的点击方法,这些都是自己的功能
    //    layer动画主要就是给空间的layer添加动画来实现效果
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    //设置动画起始的值
    animation.fromValue = [NSNumber numberWithFloat:0];
    //设置最终旋转的角度
    animation.toValue = [NSNumber numberWithFloat:M_PI * 2];
    //设置动画的播放时长
    animation.duration =10;
    //设置动画的执行次数,整数最大值
    animation.repeatCount = NSIntegerMax;
    //旋转之后是否要回到原来的位置
    animation.autoreverses=NO;
    // 是否照结束为止继续旋转
    animation.cumulative =YES;
    [self.bigImageView.layer addAnimation:animation forKey:@"rotate"];
}

#pragma mark 开始按钮   开始停止方法
-(void)butotn:(UIButton *)button
{
    //关键帧动画
    //用动画完成放大的一个效果
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    //需要给他设置一个关键帧的值,这个值就是变化过程
    animation.values = @[@(0.5),@(1),@(1.5)];
    //设置动画的时长
    animation.duration =0.2;
    //加到button上
    [self.playButton.layer addAnimation:animation forKey:@"animation"];
    //根据状态更换图片
    if (self.isStart == NO)
    {
        
        [self.player pause];
        [self.playButton setImage:[UIImage imageNamed:@"kai.png"] forState:UIControlStateNormal];
        //每一个view的layer层系统都有一个记录时间的属性,通过改变view的动画时间来控制动画效果
        CFTimeInterval stop =[self.bigImageView.layer convertTime:CACurrentMediaTime() fromLayer:nil];
        //为了暂停,把播放速度设置为0
        self.bigImageView.layer.speed=0;
        //记录当前时间的偏移量
        self.bigImageView.layer.timeOffset =stop;
    }else
    {
        
        [self.player play];
        [self.playButton setImage:[UIImage imageNamed:@"ting.png"] forState:UIControlStateNormal];
        
        //先找到上一次挺值得时间偏移量
        CFTimeInterval time = self.bigImageView.layer.timeOffset;
        // 让动画旋转地速度恢复
        self.bigImageView.layer.speed =1;
        //把偏移量清0;
        self.bigImageView.layer.timeOffset = 0;
        self.bigImageView.layer.beginTime = 0;
        //接下来以停止时间作为开始,继续旋转
        self.bigImageView.layer.beginTime = [self .bigImageView.layer convertTime:CACurrentMediaTime() fromLayer:nil]-time;
    }
    self.isStart = !self.isStart;
    
}

#pragma mark addProgress
-(void)addProgress
{
    //设置成(1.1)每秒执行一次
    [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        //当前视频的总时间,将CMTime转换成CGFloat
        CGFloat durationtime = CMTimeGetSeconds(self.playerItem.duration);
        
        //当前时间
        CGFloat current  =CMTimeGetSeconds(self.player.currentTime);
        //倒计时
        //        CGFloat rem =durationtime -current;
        
        //把时间转换成NSString,在进行赋值
        NSString *tltalet =[NSString stringWithFormat:@"%02d:%02d",(int)current/60,(int)current%60];
        NSString *currentT =[NSString stringWithFormat:@"%02d:%02d",(int)durationtime/60,(int)durationtime%60];
        NSString *timeStr =[NSString stringWithFormat:@"%@/%@",tltalet,currentT];
        //最后给label进行赋值
        self.timeLabel.text =timeStr;
        
        //让slider也向前去移动
        self.slider.value  =current/durationtime;
        
        //保存总时长,用于手动快进的时候
        self.duration = durationtime;
    }];
}

#pragma mark 左右滑动按钮点击方法
-(void)slider:(UISlider *)slider
{
    
    //获取当前的时间
    double currentTime =self.duration *self.slider.value;
    //设置要进行跳转的时间
    CMTime drage = CMTimeMake(currentTime,1);
    //对player进行设置
    [self.player seekToTime:drage completionHandler:^(BOOL finished) {
        [self.player play];
        
    }];
    
}

#pragma mark  导航栏
-(void)creatPilot
{
    self.navigationItem.title = self.titleName;
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"qwe.png"] style:1 target:self action:@selector(barButton:)];
    self.navigationItem.leftBarButtonItem.tintColor =[UIColor blackColor];
    
}
#pragma  mark 返回按钮
-(void)barButton:(UIBarButtonItem *)left
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.player  pause];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
