//
//  WYZ_DetailsViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_DetailsViewController.h"
#import "WYZ_DetailsViewModel.h"
#import "WYZ_DetailsTableViewCell.h"
#import "WYZ_NovelistenViewController.h"
#define  ZZS  @"http://mobile.ximalaya.com/mobile/others/ca/album/track/%@/true/1/20?device=iPhone&position=1&title=%%E7%%9B%%B8%%E5%%A3%%B0%%E8%%AF%%84%%E4%%B9%%A6%%E6%%8E%%92%%E8%%A1%%8C%%E6%%A6%%9C"

#define  ZZ   @"http://mobile.ximalaya.com/mobile/others/ca/album/track/%@/true/%ld/20?device=iPhone&position=1&title=%%E7%%9B%%B8%%E5%%A3%%B0%%E8%%AF%%84%%E4%%B9%%A6%%E6%%8E%%92%%E8%%A1%%8C%%E6%%A6%%9C"

@interface WYZ_DetailsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)MBProgressHUD *HUD;//小菊花
@property(nonatomic,retain)NSMutableArray *arr;
@property(nonatomic,retain)UITableView *downTabelView;//创建tabelView

@property(nonatomic,retain)UIImageView *leftImageView;// 左图片
@property(nonatomic,retain)UIView *downView;//第view
@property(nonatomic,retain)UIImageView *photoImageView;//头像
@property(nonatomic,retain)UILabel *nameLabel;//名字
@property(nonatomic,retain)UILabel *downLabel;//下名字
@property(nonatomic,assign) NSInteger page;//刷新页数


@end

@implementation WYZ_DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatHUD];//小菊花
    [self creatPilot];//导航栏设置
    [self createParsing];//数据解析
    [self creatOther];//其他控件
    [self creatTabelView];//tabelView
    
    self.page =2;
}
#pragma mark数据解析
-(void)createParsing
{
    AFHTTPSessionManager *Manager =[AFHTTPSessionManager manager];
    [Manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
    [Manager GET:[NSString stringWithFormat:ZZS,self.albumId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject[@"album"];//头视图
        self.arr =[NSMutableArray array];
        for (NSDictionary *diction in responseObject[@"tracks"][@"list"])
        {
            WYZ_DetailsViewModel *mod =[[WYZ_DetailsViewModel alloc] init];
            [mod setValuesForKeysWithDictionary:diction];
            [self.arr addObject:mod];
            
        }
        // 左图片
        [self.leftImageView setImageWithURL:[NSURL URLWithString:dic[@"coverOrigin"]]];
        //头像
        [self.photoImageView setImageWithURL:[NSURL URLWithString:dic[@"avatarPath"]]];
        //名字
        self.nameLabel.text = dic[@"nickname"];
        //下名字
        self.downLabel.text = dic[@"intro"];
        self.downLabel.numberOfLines =0;
        
        [self.downTabelView reloadData];
        
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        //           NSLog(@"%@",error);
        self.HUD.hidden = YES;
        
    }];
    
}
#pragma  mark 其它控件
-(void)creatOther
{
    //底view
    self.downView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120*HEIGHT)];
    
    // 左图片
    self.leftImageView =[[UIImageView alloc] init];
    [self.downView addSubview:self.leftImageView];
    [self.leftImageView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10*HEIGHT);
        make.left.mas_equalTo(10*WIDTH);
        make.width.mas_equalTo(100*WIDTH);
        make.height.mas_equalTo(100*HEIGHT);
    }];
    
    //头像
    self.photoImageView =[[UIImageView alloc] init];
    [self.downView addSubview:self.photoImageView];
    self.photoImageView.layer.cornerRadius =15*WIDTH;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15*HEIGHT);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(30*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    
    
    //名字
    self.nameLabel = [[UILabel alloc] init];
    [self.downView addSubview:self.nameLabel];
    self.nameLabel.font = [UIFont systemFontOfSize:17*WIDTH];
    [self.nameLabel  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15*HEIGHT);
        make.left.mas_equalTo(self.photoImageView.mas_right).offset(0);
        make.width.mas_equalTo(200*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    //下名字
    self.downLabel =[[UILabel alloc] init];
    self.downLabel.font =[UIFont systemFontOfSize:17*WIDTH];
    [self.downView addSubview:self.downLabel];
    [self.downLabel  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(self.leftImageView.mas_right).offset(10*WIDTH);
        make.width.mas_equalTo(300*WIDTH);
        make.height.mas_equalTo(80*HEIGHT);
    }];
    
}
#pragma mark tabelView
-(void)creatTabelView
{
    self.downTabelView =[[UITableView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,SCREEN_HEIGHT-64) style:UITableViewStylePlain];
    //    self.downTabelView.backgroundColor =[UIColor redColor];
    [self.view addSubview:self.downTabelView];
    self.downTabelView.rowHeight =70*HEIGHT;
    self.downTabelView.dataSource =self;
    self.downTabelView.delegate = self;
    self.downTabelView.tableHeaderView = self.downView;
    [self.downTabelView registerClass:[WYZ_DetailsTableViewCell class] forCellReuseIdentifier:@"reuse"];
    ////添加尾部控件加载的方法
    self.downTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    
}
#pragma mark  上拉加载
-(void)footerRereshing
{
    AFHTTPSessionManager *Manager =[AFHTTPSessionManager manager];
    [Manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
    [Manager GET:[NSString stringWithFormat:ZZ,self.albumId,self.page] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject[@"album"];//头视图
        
        for (NSDictionary *diction in responseObject[@"tracks"][@"list"])
        {
            WYZ_DetailsViewModel *mod =[[WYZ_DetailsViewModel alloc] init];
            [mod setValuesForKeysWithDictionary:diction];
            [self.arr addObject:mod];
        }
        // 左图片
        [self.leftImageView setImageWithURL:[NSURL URLWithString:dic[@"coverOrigin"]]];
        //头像
        [self.photoImageView setImageWithURL:[NSURL URLWithString:dic[@"avatarPath"]]];
        //名字
        self.nameLabel.text = dic[@"nickname"];
        //下名字
        self.downLabel.text = dic[@"intro"];
        self.downLabel.numberOfLines =0;
        [self.downTabelView reloadData];
        
        [self.downTabelView.mj_footer  endRefreshing];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        //           NSLog(@"%@",error);
        self.HUD.hidden = YES;
        
    }];
    self.page++;
}
#pragma mark  点击方法
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_DetailsViewModel *dvm =self.arr[indexPath.row];
    WYZ_NovelistenViewController *nvc =[[WYZ_NovelistenViewController alloc] init];
    nvc.titleName = dvm.title;
    nvc.nickname =dvm.nickname;
    nvc.smallLogo = dvm.smallLogo;
    nvc.playUrl64 = dvm.playUrl64;
    nvc.coverLarge = dvm.coverLarge;
    [self.navigationController pushViewController:nvc  animated:YES];
    //    NSLog(@"########%@",nvc.playUrl64);
}
#pragma mark  几个cell
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arr.count;
}
#pragma mark 创建cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_DetailsViewModel *model =self.arr[indexPath.row];
    WYZ_DetailsTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"reuse"];
    //    cell.backgroundColor =[UIColor colorWithRed:1.000 green:0.000 blue:0.502 alpha:1.000];
    [cell.leftImageView setImageWithURL:[NSURL URLWithString:model.albumImage]];
    cell.nameLabel.text = model.title;
    //    CGFloat  fl=[model.playtimes  floatValue]/10000;
    //    cell.playPeopleLabel.text =[NSString stringWithFormat:@"%0.1f万",fl];
    cell.playPeopleLabel.text =[NSString stringWithFormat:@"%@万",model.playtimes];
    cell.commentPeopleLabel.text =[NSString stringWithFormat:@"%@",model.comments];
    
    
    return cell;
    
}

#pragma mark 小菊花
-(void)creatHUD
{
    
    self.HUD =[MBProgressHUD showHUDAddedTo:self.view  animated:YES];
    self.HUD.labelText = @"正在努力为您加载,请稍后";
}
#pragma mark  导航栏
-(void)creatPilot
{
    self.view.backgroundColor =[UIColor whiteColor];
    self.navigationItem.title = self.name;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"qwe.png"] style:1 target:self action:@selector(barButton:)];
        self.navigationController.navigationBar.barTintColor  = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
}
#pragma mark button  点击方法
-(void)barButton:(UIButton *)button
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
