//
//  WYZ_CrosstalkViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_CrosstalkViewController.h"
#import "WYZ_CrosstalkModel.h"
#import "BannarView.h"
#import "WYZ_TOPtabelViewcell.h"
#import "WYZ_CrosstalkTabelCell.h"
#import "WYZ_HeadingCollectionCell.h"
#import "WYZ_TOPViewController.h"
#import "WYZ_DetailsViewController.h"
#import "WYZ_SlideModel.h"
#define   ANALYSIS @"http://mobile.ximalaya.com/mobile/discovery/v2/category/recommends?categoryId=12&contentType=album&device=iPhone&position=&scale=2&title=%E6%9B%B4%E5%A4%9A&version=4.3.38"
#import "WYZ_SeekViewController.h"

@interface WYZ_CrosstalkViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,BannarView>
@property(nonatomic,retain)MBProgressHUD *HUD;//小菊花
@property(nonatomic,retain)NSMutableArray *array; //轮播图
@property(nonatomic,retain)BannarView *bannarView; //自定义轮播图
@property(nonatomic,retain)UITableView *tabelView;//tabelView
@property(nonatomic,retain)NSMutableArray *arr; //tabel
@property(nonatomic,retain)UICollectionView *collectionView;//瀑布流
@property(nonatomic,retain)NSMutableArray *topArr;//上collecitonView 个数
@property(nonatomic,retain)UICollectionView *downCollection;//底下滑动collection

@property(nonatomic,retain)NSMutableArray *muArr;
@property(nonatomic,retain)NSString *str;

//点击collectionView动画效果需要的属性
@property (nonatomic,retain) NSIndexPath *indexPath;
@property(nonatomic,assign)NSInteger page;


@property(nonatomic,retain)UITableView *GDGtabelView;
@property(nonatomic,retain)UITableView *TJtabelView;
@property(nonatomic,retain)UITableView *ZBStabelView;
@property(nonatomic,retain)UITableView *STFabelView;
@property(nonatomic,retain)UITableView *YKCabelView;
@property(nonatomic,retain)UITableView *DYSabelView;
@property(nonatomic,retain)UITableView *XStabelView;
@property(nonatomic,retain)UITableView *PStabelView;
@property(nonatomic,retain)UITableView *XPtabelView;
@property(nonatomic,retain)UITableView *XSMJtabelView;
@property(nonatomic,retain)UITableView *XPWtabelView;
@property(nonatomic,retain)UITableView *LLFabelView;
@property(nonatomic,retain)UITableView *TLYabelView;
@property(nonatomic,retain)UITableView *FYCabelView;
@end

@implementation WYZ_CrosstalkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self creatHUD];//小菊花
    [self creatPilot];//导航栏设置
    [self  creatAnalysis];//解析数据
    [self creatCollectiopnView];//滑动瀑布流
    [self creatDownCollection];//底下滑动collectionView
    [self creatOtherTabelView];//其余tabelView
    [self creatTabelView];//tabelView
    self.bannarView = [[BannarView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,180*HEIGHT)];
    self.bannarView.backgroundColor =[UIColor yellowColor];
    self.bannarView.deldete = self;
    self.tabelView.tableHeaderView = self.bannarView;
    self.page = 2;
    
}
#pragma mark  轮播图协议方法
-(void)string:(NSString *)str
{
    WYZ_DetailsViewController *dvc =[[WYZ_DetailsViewController alloc] init];
    dvc.albumId =str;
    [self.navigationController  pushViewController:dvc animated:YES];
}

#pragma mark 数据解析
-(void)creatAnalysis
{
    AFHTTPSessionManager  *manager =[AFHTTPSessionManager manager];
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
    [manager  GET:ANALYSIS parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableDictionary *dic =responseObject[@"focusImages"];
        NSMutableDictionary *dicc =responseObject[@"categoryContents"];
        //轮播图
        self.array =[NSMutableArray array];
        for (NSMutableDictionary  *diction in dic[@"list"])
        {
            WYZ_CrosstalkModel  *mod =[[WYZ_CrosstalkModel alloc] init];
            [mod setValuesForKeysWithDictionary:diction];
            [self.array addObject:mod];
        }
        self.bannarView.picArr = self.array;
        //tabelView
        self.arr =[NSMutableArray array];
        for (NSMutableDictionary *diction in dicc[@"list"])
        {
            WYZ_CrosstalkModel  *mod =[[WYZ_CrosstalkModel alloc] init];
            [mod setValuesForKeysWithDictionary:diction];
            [self.arr addObject:mod];
        }
        //collecotionView
        self.topArr =[NSMutableArray array];
        for (NSDictionary *dic in responseObject[@"tags"][@"list"])
        {
            WYZ_CrosstalkModel *model =[[WYZ_CrosstalkModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.topArr addObject:model.tname];
        }
        
        [self.topArr insertObject:@"推荐" atIndex:0];
        [self.collectionView reloadData];
        [self.tabelView reloadData];
        self.HUD.hidden = YES;
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        self.HUD.hidden = YES;
        
    }];
    
}

#pragma mark 底下滑动collectionView
-(void)creatDownCollection
{
    UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(SCREEN_WIDTH,SCREEN_HEIGHT);
    flowLayout.minimumLineSpacing =0;  //行间距
    flowLayout.minimumInteritemSpacing =0;//列间距
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//就四周距离
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.downCollection =[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:flowLayout];
    [self.downCollection setCollectionViewLayout:flowLayout]; //添加一个UICollectionViewFlowLayout
    [self.view addSubview:self.downCollection];
    self.downCollection.dataSource =self;
    self.downCollection.delegate =self;
    self.downCollection.bounces = YES; //回弹
    self.downCollection.pagingEnabled = YES;//按页
    self.downCollection.showsHorizontalScrollIndicator = NO;
    self.downCollection.showsVerticalScrollIndicator =NO;
    //    self.downCollection.contentOffset =CGPointMake(SCREEN_WIDTH * 10, 0);
    self.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.downCollection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.collectionView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(SCREEN_HEIGHT);
        
    }];
    [self.downCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"downCollection"];
}
#pragma mark 滑动瀑布流
-(void)creatCollectiopnView
{
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(70*WIDTH,40*HEIGHT);
    flowLayout.minimumLineSpacing = 5*WIDTH;  //行间距
    flowLayout.minimumInteritemSpacing =5*HEIGHT;//列间距
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView =[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:flowLayout];
    [self.collectionView setCollectionViewLayout:flowLayout]; //添加一个UICollectionViewFlowLayout
    [self.view addSubview:self.collectionView];
    self.collectionView.dataSource =self;
    self.collectionView.delegate =self;
    self.collectionView.bounces = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator =NO;
    
    self.collectionView.backgroundColor =[UIColor colorWithWhite:0.971 alpha:1.000];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(1*HEIGHT);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(39*HEIGHT);
    }];
    [self.collectionView registerClass:[WYZ_HeadingCollectionCell class] forCellWithReuseIdentifier:@"HeadingCollectionCell"];
}

#pragma mark  collectionView点击方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionView)
    {
        WYZ_HeadingCollectionCell *cell = (WYZ_HeadingCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [self collectionView:self.collectionView didDeselectItemAtIndexPath:self.indexPath];
        cell.titleLabel.textColor = [UIColor redColor];
        cell.titleLabel.font = [UIFont systemFontOfSize:21*WIDTH];
        if (indexPath.row > 4 && indexPath.row <= self.topArr.count - 3) {
            CGFloat offsetX =  cell.frame.origin.x - SCREEN_WIDTH / 2  ;
            CGFloat offsetY = self.collectionView.contentOffset.y;
            CGPoint offset = CGPointMake(offsetX, offsetY);
            [self.collectionView setContentOffset:offset animated:YES];
        }
        if (self.indexPath.row > 4 && indexPath.row <= 4) {
            [self.collectionView setContentOffset:CGPointZero animated:YES];
        }
        if (self.indexPath.row <= self.topArr.count - 2 && indexPath.row >= self.topArr.count - 2) {
            WYZ_HeadingCollectionCell *lastCell = (WYZ_HeadingCollectionCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.topArr.count - 1 inSection:0]];
            
            [self.collectionView setContentOffset:CGPointMake( lastCell.frame.origin.x + lastCell.frame.size.width - SCREEN_WIDTH , 0 ) animated:YES];
        }
        
        [self.downCollection setContentOffset: CGPointMake(SCREEN_WIDTH * indexPath.row, 0) animated:YES];
        self.indexPath = indexPath;
        
        self.str =self.topArr[indexPath.row];
        [self  creatSJjx];//数据解析
    }
}
#pragma mark 点击cell滑动等效果
// 选中状态
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.collectionView || collectionView == self.downCollection) {
        return YES;
    }else{
        return NO;
    }
}

// 取消状态
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.collectionView || collectionView == self.downCollection) {
        WYZ_HeadingCollectionCell *cell = (WYZ_HeadingCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.titleLabel.font = [UIFont systemFontOfSize:17*WIDTH];
        cell.titleLabel.textColor = [UIColor blackColor];
    }
}
#pragma mark 滚动结束执行的方法
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.downCollection) {
        NSInteger index =  self.downCollection.contentOffset.x / SCREEN_WIDTH;
        WYZ_HeadingCollectionCell *cell = (WYZ_HeadingCollectionCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        [self collectionView:self.collectionView didDeselectItemAtIndexPath:self.indexPath];
        
        cell.titleLabel.font = [UIFont systemFontOfSize:21*WIDTH];
        cell.titleLabel.textColor = [UIColor redColor];
        
        if (index > 4 && index <= self.topArr.count - 2) {
            CGFloat offsetX =  cell.frame.origin.x - SCREEN_WIDTH / 2  ;
            CGPoint offset = CGPointMake(offsetX, 0);
            [self.collectionView setContentOffset:offset animated:YES];
        }
        if (self.indexPath.row > 4 && index <= 4) {
            [self.collectionView setContentOffset:CGPointZero animated:YES];
        }
        if (self.indexPath.row < self.topArr.count - 2 && index >= self.topArr.count - 2) {
            WYZ_HeadingCollectionCell *lastCell = (WYZ_HeadingCollectionCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.topArr.count - 1 inSection:0]];
            [self.collectionView setContentOffset:CGPointMake( lastCell.frame.origin.x + lastCell.frame.size.width - SCREEN_WIDTH  , self.collectionView.contentOffset.y) animated:YES];
        }
        
        self.indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        self.str = self.topArr[index];
        [self  creatSJjx];//数据解析
    }
    
}
#pragma mark flowLayout自适应
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _collectionView)
    {
        NSString *nameStr = _topArr[indexPath.row];
        return CGSizeMake(nameStr.length * 22 * WIDTH, 40 * HEIGHT);
    }else if (collectionView == _downCollection)
    {
        return CGSizeMake(SCREEN_WIDTH,SCREEN_HEIGHT);
    }else{
        return CGSizeZero;
    }
}
#pragma mark  几个item
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView == self.collectionView)
    {
        return self.topArr.count;
    }else
    {
        return 15;
    }
    
}
#pragma mark  创建UICollectionViewCell
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.collectionView)
    {
        WYZ_HeadingCollectionCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"HeadingCollectionCell" forIndexPath:indexPath];
        cell.titleLabel.textColor = [UIColor blackColor];
        cell.titleLabel.text =self.topArr[indexPath.row];
        
        cell.titleLabel.frame = CGRectMake(0, 0,cell.titleLabel.text.length * 23*WIDTH, 50*HEIGHT);
        //        cell.backgroundColor = [UIColor orangeColor];
        if (self.indexPath == indexPath)
        {
            cell.titleLabel.font = [UIFont systemFontOfSize:21*WIDTH];
            cell.titleLabel.textColor = [UIColor redColor];
        }else
        {
            cell.titleLabel.font = [UIFont systemFontOfSize:17*WIDTH];
            cell.titleLabel.textColor = [UIColor blackColor];
        }
        
        return cell;
        
    }else
    {
        UICollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"downCollection" forIndexPath:indexPath];
        cell.backgroundColor =[UIColor cyanColor];
        
        return cell;
    }
}
///数据解析
-(void)creatSJjx
{
    NSLog(@"***滑动点击有名字***%@",self.str);
    
    // url一般有字母,数字和特殊的符号比如/,&,?,=,$组成,所以一般的url里没有汉子的,所有的汉子在请求的时候需要给他进行转码
    // ios9.0之后,转码的方法也发生了变化,采用字符集合的方式对网址进行转码操作
    self.str =[self.str  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString: self.str ]];
    NSLog(@"***--***%@",self.str);
    if ([ self.str isEqualToString:@"%E6%8E%A8%E8%8D%90"])
    {
        AFHTTPSessionManager  *manager =[AFHTTPSessionManager manager];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
        
        [manager  GET:ANALYSIS parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSMutableDictionary *dic =responseObject[@"focusImages"];
            NSMutableDictionary *dicc =responseObject[@"categoryContents"];
            //轮播图
            self.array =[NSMutableArray array];
            for (NSMutableDictionary  *diction in dic[@"list"])
            {
                WYZ_CrosstalkModel  *mod =[[WYZ_CrosstalkModel alloc] init];
                [mod setValuesForKeysWithDictionary:diction];
                [self.array addObject:mod];
            }
            //tabelView
            self.arr =[NSMutableArray array];
            for (NSMutableDictionary *diction in dicc[@"list"])
            {
                WYZ_CrosstalkModel  *mod =[[WYZ_CrosstalkModel alloc] init];
                [mod setValuesForKeysWithDictionary:diction];
                [self.arr addObject:mod];
            }
            //collecotionView
            self.topArr =[NSMutableArray array];
            for (NSDictionary *dic in responseObject[@"tags"][@"list"])
            {
                WYZ_CrosstalkModel *model =[[WYZ_CrosstalkModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [self.topArr addObject:model.tname];
            }
            
            [self.topArr insertObject:@"推荐" atIndex:0];
            [self.collectionView reloadData];
            [self.tabelView reloadData];
            self.bannarView.picArr = self.array;
            self.HUD.hidden = YES;
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
            //显示alertController
            [self presentViewController:alert animated:YES completion:^{}];
            //添加button
            UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:action];
            self.HUD.hidden = YES;
        }];
    }else
    {
        AFHTTPSessionManager  *manager =[AFHTTPSessionManager manager];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
        
        [manager  GET:[NSString stringWithFormat:@"http://mobile.ximalaya.com/mobile/discovery/v1/category/album?calcDimension=hot&categoryId=12&device=iPhone&pageId=1&pageSize=20&position=&status=0&tagName=%@",self.str] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            self.muArr =[NSMutableArray array];
            for (NSDictionary *dic in responseObject[@"list"])
            {
                WYZ_SlideModel *mod = [[WYZ_SlideModel alloc] init];
                [mod setValuesForKeysWithDictionary:dic];
                [self.muArr addObject:mod];
            }
            [self.GDGtabelView reloadData];
            [self.TJtabelView reloadData];
            [self.ZBStabelView reloadData];
            [self.STFabelView reloadData];
            [self.YKCabelView reloadData];
            [self.DYSabelView reloadData];
            [self.XStabelView  reloadData];
            [self.PStabelView reloadData];
            [self.XPtabelView reloadData];
            [self.XSMJtabelView reloadData];
            [self.XPWtabelView reloadData];
            [self.LLFabelView reloadData];
            [self.TLYabelView reloadData];
            [self.FYCabelView reloadData];
            self.HUD.hidden = YES;
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
            //显示alertController
            [self presentViewController:alert animated:YES completion:^{}];
            //添加button
            UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:action];
            self.HUD.hidden = YES;
            
        }];
        
    }
    
}
////**************
#pragma mark tableView
-(void)creatOtherTabelView
{
    self.GDGtabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.GDGtabelView];
    self.GDGtabelView.rowHeight =100*HEIGHT;
    self.GDGtabelView.dataSource =self;
    self.GDGtabelView.delegate  =self;
    [self.GDGtabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.TJtabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*2, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.TJtabelView];
    self.TJtabelView.rowHeight =100*HEIGHT;
    self.TJtabelView.dataSource =self;
    self.TJtabelView.delegate  =self;
    [self.TJtabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.ZBStabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*3, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.ZBStabelView];
    self.ZBStabelView.rowHeight =100*HEIGHT;
    self.ZBStabelView.dataSource =self;
    self.ZBStabelView.delegate  =self;
    [self.ZBStabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.STFabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*4, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.STFabelView];
    self.STFabelView.rowHeight =100*HEIGHT;
    self.STFabelView.dataSource =self;
    self.STFabelView.delegate  =self;
    [self.STFabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.YKCabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*5, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.YKCabelView];
    self.YKCabelView.rowHeight =100*HEIGHT;
    self.YKCabelView.dataSource =self;
    self.YKCabelView.delegate  =self;
    [self.YKCabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.DYSabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*6, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.DYSabelView];
    self.DYSabelView.rowHeight =100*HEIGHT;
    self.DYSabelView.dataSource =self;
    self.DYSabelView.delegate  =self;
    [self.DYSabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.XStabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*7, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.XStabelView];
    self.XStabelView.rowHeight =100*HEIGHT;
    self.XStabelView.dataSource =self;
    self.XStabelView.delegate  =self;
    [self.XStabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.PStabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*8, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.PStabelView];
    self.PStabelView.rowHeight =100*HEIGHT;
    self.PStabelView.dataSource =self;
    self.PStabelView.delegate  =self;
    [self.PStabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.XPtabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*9, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.XPtabelView];
    self.XPtabelView.rowHeight =100*HEIGHT;
    self.XPtabelView.dataSource =self;
    self.XPtabelView.delegate  =self;
    [self.XPtabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.XSMJtabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*10, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.XSMJtabelView];
    self.XSMJtabelView.rowHeight =100*HEIGHT;
    self.XSMJtabelView.dataSource =self;
    self.XSMJtabelView.delegate  =self;
    [self.XSMJtabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.XPWtabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*11, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.XPWtabelView];
    self.XPWtabelView.rowHeight =100*HEIGHT;
    self.XPWtabelView.dataSource =self;
    self.XPWtabelView.delegate  =self;
    [self.XPWtabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.LLFabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*12, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.LLFabelView];
    self.LLFabelView.rowHeight =100*HEIGHT;
    self.LLFabelView.dataSource =self;
    self.LLFabelView.delegate  =self;
    [self.LLFabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.TLYabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*13, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.TLYabelView];
    self.TLYabelView.rowHeight =100*HEIGHT;
    self.TLYabelView.dataSource =self;
    self.TLYabelView.delegate  =self;
    [self.TLYabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    self.FYCabelView =[[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*14, 0,SCREEN_WIDTH, SCREEN_HEIGHT-114-39*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.FYCabelView];
    self.FYCabelView.rowHeight =100*HEIGHT;
    self.FYCabelView.dataSource =self;
    self.FYCabelView.delegate  =self;
    [self.FYCabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    
    [self creatShuaxin];
    
}
-(void)creatShuaxin
{

    self.GDGtabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.GDGtabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.TJtabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.TJtabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.STFabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.STFabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.YKCabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.YKCabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.DYSabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.DYSabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.XStabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.XStabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.PStabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.PStabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.XPtabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.XPtabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.XSMJtabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.XSMJtabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    
    }
#pragma  mark 头部下拉刷新
-(void)headerRereshing
{
    
    AFHTTPSessionManager  *manager =[AFHTTPSessionManager manager];
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
    
    [manager  GET:[NSString stringWithFormat:@"http://mobile.ximalaya.com/mobile/discovery/v1/category/album?calcDimension=hot&categoryId=12&device=iPhone&pageId=1&pageSize=20&position=&status=0&tagName=%@",self.str] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.muArr =[NSMutableArray array];
        for (NSDictionary *dic in responseObject[@"list"])
        {
            WYZ_SlideModel *mod = [[WYZ_SlideModel alloc] init];
            [mod setValuesForKeysWithDictionary:dic];
            [self.muArr addObject:mod];
        }
        [self.GDGtabelView reloadData];
        [self.TJtabelView reloadData];
        [self.ZBStabelView reloadData];
        [self.STFabelView reloadData];
        [self.YKCabelView reloadData];
        [self.DYSabelView reloadData];
        [self.XStabelView  reloadData];
        [self.PStabelView reloadData];
        [self.XPtabelView reloadData];
        [self.XSMJtabelView reloadData];
        [self.XPWtabelView reloadData];
        [self.LLFabelView reloadData];
        [self.TLYabelView reloadData];
        [self.FYCabelView reloadData];
        [self crearStop];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        self.HUD.hidden = YES;
        
    }];
    
}
-(void)crearStop
{
    [self.GDGtabelView.mj_header endRefreshing];
    
    
    [self.TJtabelView.mj_header endRefreshing];
    [self.ZBStabelView.mj_header endRefreshing];
    [self.STFabelView.mj_header endRefreshing];
    [self.YKCabelView.mj_header endRefreshing];
    [self.DYSabelView.mj_header endRefreshing];
    [self.XStabelView.mj_header endRefreshing];
    [self.PStabelView.mj_header endRefreshing];
    [self.XPtabelView.mj_header endRefreshing];
    [self.XSMJtabelView.mj_header endRefreshing];
    [self.XPWtabelView.mj_header endRefreshing];
    [self.LLFabelView.mj_header endRefreshing];
    [self.TLYabelView.mj_header endRefreshing];
    [self.FYCabelView.mj_header endRefreshing];
}
#pragma mark  尾部 加载
-(void)footerRereshing
{
    AFHTTPSessionManager  *manager =[AFHTTPSessionManager manager];
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript",@"text/html",@"text/css", @"text/plain", @"application/x-javascript", @"application/javascript",nil]];
    [manager  GET:[NSString stringWithFormat:@"http://mobile.ximalaya.com/mobile/discovery/v1/category/album?calcDimension=hot&categoryId=12&device=iPhone&pageId=%ld&pageSize=20&position=&status=0&tagName=%@",self.page,self.str] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        for (NSDictionary *dic in responseObject[@"list"])
        {
            WYZ_SlideModel *mod = [[WYZ_SlideModel alloc] init];
            [mod setValuesForKeysWithDictionary:dic];
            [self.muArr addObject:mod];
        }
        [self.GDGtabelView reloadData];
        [self.TJtabelView reloadData];
        [self.ZBStabelView reloadData];
        [self.STFabelView reloadData];
        [self.YKCabelView reloadData];
        [self.DYSabelView reloadData];
        [self.XStabelView  reloadData];
        [self.PStabelView reloadData];
        [self.XPtabelView reloadData];
        [self.XSMJtabelView reloadData];
        [self.XPWtabelView reloadData];
        [self.LLFabelView reloadData];
        [self.TLYabelView reloadData];
        [self.FYCabelView reloadData];
        [self crearStopdown];
        self.HUD.hidden = YES;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"网络延迟" message:@"网络重新连接中"preferredStyle:UIAlertControllerStyleAlert];
        //显示alertController
        [self presentViewController:alert animated:YES completion:^{}];
        //添加button
        UIAlertAction * action=[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action];
        self.HUD.hidden = YES;
        
    }];
    
    self.page++;
}
-(void)crearStopdown
{
    [self.GDGtabelView.mj_footer endRefreshing];
    [self.TJtabelView.mj_footer endRefreshing];
    [self.STFabelView.mj_footer endRefreshing];
    [self.YKCabelView.mj_footer endRefreshing];
    [self.DYSabelView.mj_footer endRefreshing];
    [self.XStabelView.mj_footer endRefreshing];
    [self.PStabelView.mj_footer endRefreshing];
    [self.XPtabelView.mj_footer endRefreshing];
    [self.XSMJtabelView.mj_footer endRefreshing];
    
}
////**************
#pragma mark tabelView
-(void)creatTabelView
{
    self.tabelView =[[UITableView alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT-90*HEIGHT) style:UITableViewStylePlain];
    [self.downCollection addSubview:self.tabelView];
    self.tabelView.rowHeight =100*HEIGHT;
    self.tabelView.dataSource =self;
    self.tabelView.delegate  =self;
    [self.tabelView registerClass:[WYZ_CrosstalkTabelCell class] forCellReuseIdentifier:@"reuse"];
    [self.tabelView registerClass:[WYZ_TOPtabelViewcell class] forCellReuseIdentifier:@"reus"];
    
}

#pragma mark 分区标题高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }else
    {
        return  30*HEIGHT;
    }
}
#pragma mark 分区标题铺view  随便铺 控件
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    WYZ_CrosstalkModel *mod =self.arr[section];
    UIView *vie=[[UIView alloc] initWithFrame:CGRectMake(0,0 ,SCREEN_WIDTH, 0)];
    vie.backgroundColor=[UIColor whiteColor];
    
    UIImageView *leftImageView =[[UIImageView alloc] init];
    leftImageView.image =[UIImage imageNamed:@"zxcv.png"];
    [vie addSubview:leftImageView];
    [leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10*WIDTH);
        make.top.mas_equalTo(8*HEIGHT);
        make.width.mas_equalTo(15*WIDTH);
        make.height.mas_equalTo(15*HEIGHT);
    }];
    
    
    UILabel *label =[[UILabel alloc] init];
    label.text = mod.title;
    label.font =[UIFont systemFontOfSize:17*WIDTH];
    label.textColor =[UIColor blackColor];
    [vie addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView.mas_right).offset(2*WIDTH);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(70*WIDTH);
        make.height.mas_equalTo(30*HEIGHT);
    }];
    
    return  vie;
}
#pragma mark  cell点击方法
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tabelView)
    {
        if (indexPath.row == 0 )
        {
            WYZ_TOPViewController *TVC =[[WYZ_TOPViewController alloc] init];
            [self.navigationController pushViewController:TVC  animated:YES];
        }else
        {
            WYZ_DetailsViewController *dvc =[[WYZ_DetailsViewController alloc] init];
            WYZ_CrosstalkModel *model = self.arr[indexPath.section];
            dvc.albumId = model.list[indexPath.row][@"albumId"];
            dvc.name = model.list[indexPath.row][@"title"];
            [self.navigationController pushViewController:dvc animated:YES];
            NSLog(@"第一页***%@",dvc.albumId);
        }
    }
    else
    {
        WYZ_DetailsViewController *dvc =[[WYZ_DetailsViewController alloc] init];
        WYZ_SlideModel *mod = self.muArr[indexPath.row];
        dvc.albumId = mod.albumId;
        [self.navigationController pushViewController:dvc animated:YES];
    }
}

#pragma mark 几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tabelView)
    {
        return self.arr.count;
    }else
    {
        return 1;
    }
    
    
}
#pragma mark  几个cell
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tabelView)
    {
        return [self.arr[section]list].count;
    }
    else
    {
        return self.muArr.count;
    }
}
#pragma mark  创建cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tabelView)
    {
        WYZ_CrosstalkModel *mod =self.arr[indexPath.section];
        if (indexPath.section == 0)
        {
            WYZ_TOPtabelViewcell *cell =[tableView dequeueReusableCellWithIdentifier:@"reus"];
            [cell.leftImageView setImageWithURL:[NSURL URLWithString: mod.list[indexPath.row][@"coverPath"]]];
            cell.topLabel.text =mod.list[indexPath.section][@"title"];
            cell.downLabel.text =mod.list[indexPath.section][@"subtitle"];
            //   点击cell的效果
            cell.selectionStyle = UITableViewScrollPositionNone;
            return cell;
        }else
        {
            WYZ_CrosstalkTabelCell *cell =[tableView dequeueReusableCellWithIdentifier:@"reuse"];
            
            cell.topLabel.text = mod.list[indexPath.row][@"title"];
            
            cell.centreLabel.text = mod.list[indexPath.row][@"intro"];
            
            cell.PlayBluesLabel.text =[NSString stringWithFormat:@"%@集",mod.list[indexPath.row][@"tracksCounts"]];
            
            cell.playsLabel.text =[NSString stringWithFormat:@"%.1f万",[mod.list[indexPath.row][@"playsCounts"] floatValue]/10000];
            
            [cell.leftImageView setImageWithURL:[NSURL URLWithString: mod.list[indexPath.row][@"albumCoverUrl290"]]];
            
            //   点击cell的效果
            cell.selectionStyle = UITableViewScrollPositionNone;
            return cell;
        }
    }
    else
    {
        WYZ_SlideModel *mod =self.muArr[indexPath.row];
        WYZ_CrosstalkTabelCell *cell =[tableView dequeueReusableCellWithIdentifier:@"reuse"];
        [cell.leftImageView setImageWithURL:[NSURL URLWithString:mod.albumCoverUrl290]];
        cell.topLabel.text = mod.title;
        
        cell.centreLabel.text = mod.intro;
        
        cell.PlayBluesLabel.text =[NSString stringWithFormat:@"%@集",mod.tracksCounts];
        cell.playsLabel.text = [NSString stringWithFormat:@"%0.1f万",[mod.playsCounts  floatValue]/10000];
        return  cell;
        
    }
}

#pragma mark 小菊花
-(void)creatHUD
{
    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.HUD.labelText =@"正在努力加载中,请稍候";
}
#pragma mark  导航栏设置
-(void)creatPilot
{
    self.view.backgroundColor =[UIColor whiteColor];
    UILabel *label =[[UILabel alloc] initWithFrame:CGRectMake(64, 0, 70, 50)];
    label.text = @"听相声";
    self.navigationItem.titleView =label;
    self.navigationController.navigationBar.barTintColor  = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    self.navigationController.navigationBar.translucent = NO;
    
    UIView *view =[[UIView alloc] init];
    view.backgroundColor =[UIColor colorWithWhite:0.902 alpha:1.000];
    [self.view addSubview:view];
    [view  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(1*HEIGHT);
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-sousuo.png"] style:1 target:self action:@selector(rightButton)];
    self.navigationItem.rightBarButtonItem.tintColor =[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-fanhui.png"] style:1 target:self action:@selector(barButton:)];
      self.navigationItem.leftBarButtonItem.tintColor =[UIColor whiteColor];
}
-(void)barButton:(UIButton *)button
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)rightButton
{
    WYZ_SeekViewController *lvc =[[WYZ_SeekViewController alloc] init];
    [self.navigationController pushViewController:lvc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
