//
//  WYZ_DetailsViewController.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseViewController.h"

@interface WYZ_DetailsViewController : WYZ_BaseViewController
@property(nonatomic,retain)NSString *albumId;
@property(nonatomic,retain)NSString *name;
@end
