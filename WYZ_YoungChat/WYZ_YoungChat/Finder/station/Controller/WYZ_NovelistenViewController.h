//
//  WYZ_NovelistenViewController.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseViewController.h"

@interface WYZ_NovelistenViewController : WYZ_BaseViewController
@property(nonatomic,copy)NSString *titleName;
@property(nonatomic,copy)NSString *nickname;//名字smallLogo
@property(nonatomic,copy)NSString *smallLogo;//头像
@property(nonatomic,copy)NSString *playUrl64;//mp4
@property(nonatomic,copy)NSString *coverLarge;//图片
@end
