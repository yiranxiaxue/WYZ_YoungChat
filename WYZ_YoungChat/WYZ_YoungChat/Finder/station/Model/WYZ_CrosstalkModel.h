//
//  WYZ_CrosstalkModel.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseModel.h"

@interface WYZ_CrosstalkModel : WYZ_BaseModel
@property(nonatomic,copy)NSString *pic;


@property(nonatomic,copy)NSString *title;
@property(nonatomic,retain)NSArray  *list;


@property(nonatomic,copy)NSString *tname;


@property(nonatomic,copy)NSString *albumId;
@end
