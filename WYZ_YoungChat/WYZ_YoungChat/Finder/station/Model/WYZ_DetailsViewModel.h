//
//  WYZ_DetailsViewModel.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseModel.h"

@interface WYZ_DetailsViewModel : WYZ_BaseModel
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *albumImage;
@property(nonatomic,copy)NSString *playtimes;
@property(nonatomic,copy)NSString *comments;
@property(nonatomic,copy)NSString *albumTitle;

@property(nonatomic,copy)NSString *nickname;//名字smallLogo
@property(nonatomic,copy)NSString *smallLogo;//头像
@property(nonatomic,copy)NSString *playUrl64;//mp4
@property(nonatomic,copy)NSString *coverLarge;//图片
@end
