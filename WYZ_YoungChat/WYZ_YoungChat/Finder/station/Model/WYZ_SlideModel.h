//
//  WYZ_SlideModel.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseModel.h"

@interface WYZ_SlideModel : WYZ_BaseModel
@property(nonatomic,copy)NSString *albumCoverUrl290;
@property(nonatomic,copy)NSString *intro;
@property(nonatomic,copy)NSString *playsCounts;
@property(nonatomic,copy)NSString *tracksCounts;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *albumId;
@end
