//
//  WYZ_SeekModel.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseModel.h"

@interface WYZ_SeekModel : WYZ_BaseModel
@property(nonatomic,retain)NSString *keyword;
@property(nonatomic,retain)NSString *category;
@property(nonatomic,retain)NSString *imgPath;
@property(nonatomic,retain)NSString  *DLid;
@end
