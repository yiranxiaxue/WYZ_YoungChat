//
//  WYZ_TOPModel.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseModel.h"

@interface WYZ_TOPModel : WYZ_BaseModel
@property(nonatomic,copy)NSString *coverMiddle;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *intro;
@property(nonatomic,copy)NSString *tracksCounts;

@property(nonatomic,copy)NSString *albumId;
@end
