//
//  WYZ_MapViewController.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_MapViewController.h"



#import "WYZ_MapTableViewCell.h"


@interface WYZ_MapViewController ()<MAMapViewDelegate,AMapSearchDelegate,UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,AMapNaviManagerDelegate>
//地图
@property (nonatomic,strong) MAMapView *mapView;
//切换地图类型
@property (nonatomic,strong) UIImageView *mapTypeImageView;
@property (nonatomic,assign) BOOL mapType;
@property (nonatomic,strong) UIView *mapTypeView;
//打开交通图
@property (nonatomic,strong) UIImageView *trafficImageView;
@property (nonatomic,assign) BOOL traffic;

//搜索
@property (nonatomic,strong) AMapSearchAPI *search;

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong) NSMutableArray  *dataList;

@property (nonatomic,strong) UIView *searchView;

@property (nonatomic,strong) UITextField *searchTextField;
//保存当前位置的左边
@property (nonatomic,strong) MAUserLocation *userLocation;

//导航
@property (nonatomic,strong) AMapNaviManager *naviManager;

@end

@implementation WYZ_MapViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.mapType = YES;
        self.traffic = YES;
        self.dataList = [NSMutableArray array];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    //配置用户Key
      //地图
    [MAMapServices sharedServices].apiKey = @"b8cc54884dc4365d03b0ceffc260a285";
      //搜索
    [AMapSearchServices sharedServices].apiKey = @"b8cc54884dc4365d03b0ceffc260a285";
      //导航
    [AMapNaviServices sharedServices].apiKey = @"b8cc54884dc4365d03b0ceffc260a285";
   
    //地图
    [self createMapView];
    // 搜索
    [self createSearchView];
   
        
    
   
    
}
#pragma mark 地图
-(void)createMapView
{
    self.mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mapView.delegate = self;
    _mapView.mapType = MAMapTypeStandard;
    [_mapView setZoomLevel:15 animated:YES];
    [_mapView setUserTrackingMode: MAUserTrackingModeFollowWithHeading animated:YES];
    _mapView.pausesLocationUpdatesAutomatically = NO;
    _mapView.allowsBackgroundLocationUpdates = YES;
    _mapView.showsUserLocation = YES;
    _mapView.touchPOIEnabled = YES;
    //罗盘原点位置
    _mapView.compassOrigin = CGPointMake(30 * WIDTH, 75 * HEIGHT);
    //比例尺原点位置
    _mapView.scaleOrigin = CGPointMake(20 * WIDTH, SCREEN_HEIGHT - 40 * HEIGHT);
    //交通图
    _mapView.showTraffic= NO;
    [self.view addSubview:_mapView];
    
    
    //再次定位
    UIImageView *mapTrackImageView = [[UIImageView alloc] init];
    mapTrackImageView.image = [UIImage imageNamed:@"dingwei.png"];
    [_mapView addSubview:mapTrackImageView];
    mapTrackImageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.461];
    mapTrackImageView.userInteractionEnabled = YES;
    //添加手势
    UITapGestureRecognizer *mapTrackTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapTrackTapAction:)];
    
    [mapTrackImageView addGestureRecognizer:mapTrackTap];
    [mapTrackImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30 * WIDTH);
        make.bottom.mas_equalTo(-50 * HEIGHT);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
    }];
    
    //打开交通图
    self.trafficImageView = [[UIImageView alloc] init];
    _trafficImageView.image = [UIImage imageNamed:@"honglvdeng.png"];
    [_mapView addSubview:_trafficImageView];
    _trafficImageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.461];
    _trafficImageView.userInteractionEnabled = YES;
    //添加手势
    UITapGestureRecognizer *trafficTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trafficTapAction:)];
    [_trafficImageView addGestureRecognizer:trafficTap];
    [_trafficImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10 * WIDTH);
        make.top.mas_equalTo(160 * HEIGHT);
        make.width.mas_equalTo(30 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
    }];

    //缩放
    UIView *reduceView = [[UIView alloc] init];
    reduceView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.505];
    [_mapView addSubview:reduceView];
    [reduceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-110 * HEIGHT);
        make.right.mas_equalTo(- 10 * WIDTH);
        make.width.mas_equalTo(40 * WIDTH);
        make.height.mas_equalTo(81 * HEIGHT);
    }];
    //缩放手势
    
    UIImageView *enlargeImageView = [[UIImageView alloc] init];
    enlargeImageView.userInteractionEnabled = YES;
    enlargeImageView.image = [UIImage imageNamed:@"jiahao.png"];
    enlargeImageView.backgroundColor = [UIColor clearColor];
    [reduceView addSubview:enlargeImageView];
    UITapGestureRecognizer *enlargeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enlargeTapAction:)];
    [enlargeImageView addGestureRecognizer:enlargeTap];
    
    [enlargeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(2 * HEIGHT);
        make.bottom.mas_equalTo(-43 * HEIGHT);
        make.left.mas_equalTo(2 * WIDTH);
        make.right.mas_equalTo(-2 * WIDTH);
        
    }];
    UIView *carveView = [[UIView alloc] init];
    carveView.backgroundColor = [UIColor colorWithWhite:0.847 alpha:0.541];
    [enlargeImageView addSubview:carveView];
    [carveView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(40 * HEIGHT);
        make.left.mas_equalTo(5 * WIDTH);
        make.right.mas_equalTo(-5 * WIDTH);
        make.height.mas_equalTo(1 * HEIGHT);
    }];
    
    UIImageView *reduceImageView = [[UIImageView alloc] init];
    reduceImageView.image = [UIImage imageNamed:@"jianhao.png"];
    reduceImageView.backgroundColor = [UIColor clearColor];
    reduceImageView.userInteractionEnabled = YES;
    [reduceView addSubview:reduceImageView];
    UITapGestureRecognizer *reduceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reduceTapAction:)];
    [reduceImageView addGestureRecognizer:reduceTap];
    [reduceImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43 * HEIGHT);
        make.bottom.mas_equalTo(-2 * HEIGHT);
        make.left.mas_equalTo(2 * WIDTH);
        make.right.mas_equalTo(-2 * WIDTH);
    }];
    //导航
    UIImageView *amapNaviImageView = [[UIImageView alloc] init];
    amapNaviImageView.userInteractionEnabled = YES;
    amapNaviImageView.backgroundColor = [UIColor colorWithWhite:0.415 alpha:0.461];
    [_mapView addSubview: amapNaviImageView];
    //添加手势
    UITapGestureRecognizer *amapNaviTypeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(amapNaviTypeTapAction:)];
    [amapNaviImageView addGestureRecognizer:amapNaviTypeTap];
    [amapNaviImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10 * WIDTH);
        make.top.mas_equalTo(220 * HEIGHT);
        make.width.mas_equalTo(30 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
    }];
    
    
    //切换地图类型
    self.mapTypeImageView = [[UIImageView alloc] init];
    _mapTypeImageView.image = [UIImage imageNamed:@"qiehuandituleixing.png"];
    _mapTypeImageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.461];
    [_mapView addSubview:_mapTypeImageView];
    _mapTypeImageView.userInteractionEnabled = YES;
    //添加手势
    UITapGestureRecognizer *mapTypeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapTypeTapAction:)];
    [_mapTypeImageView addGestureRecognizer:mapTypeTap];
    [_mapTypeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10 * WIDTH);
        make.top.mas_equalTo(100 * HEIGHT);
        make.width.mas_equalTo(30 * WIDTH);
        make.height.mas_equalTo(30 * HEIGHT);
    }];
    self.mapTypeView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 40 * WIDTH, 130 * HEIGHT, 30 * WIDTH, 0)];
    [_mapView addSubview:_mapTypeView];
    [_mapTypeView bringSubviewToFront:_mapView];
    _mapTypeView.backgroundColor = [UIColor whiteColor];
}
#pragma mark 初始化导航管理
- (void)initNaviManager
{
    if (_naviManager == nil)
    {
        self.naviManager = [[AMapNaviManager alloc] init];
        [_naviManager setDelegate:self];
    }
}
#pragma mark 导航
-(void)amapNaviTypeTapAction:(UITapGestureRecognizer *)tap
{
 
    


}
#pragma mark 缩放
-(void)enlargeTapAction:(UITapGestureRecognizer *)tap
{
    [_mapView setZoomLevel:_mapView.zoomLevel+=1 animated:YES];
}
-(void)reduceTapAction:(UITapGestureRecognizer *)tap
{
    [_mapView setZoomLevel:_mapView.zoomLevel-=1 animated:YES];
   
}
#pragma mark 打开交通图
-(void)trafficTapAction:(UITapGestureRecognizer *)tap
{
    if (_traffic == YES) {
        _trafficImageView.image = [UIImage imageNamed:@"lianghonglvdeng.png"];
        _mapView.showTraffic= YES;
        _traffic = NO;
    }else{
        _trafficImageView.image = [UIImage imageNamed:@"honglvdeng.png"];
        _mapView.showTraffic= NO;
        _traffic = YES;
    }
}
#pragma makr 再次定位
-(void)mapTrackTapAction:(UITapGestureRecognizer *)tap
{
    [_mapView setZoomLevel:15 animated:YES];
    [_mapView setUserTrackingMode: MAUserTrackingModeFollowWithHeading animated:YES];
}
#pragma mark 点击切换地图类型按钮弹出界面
-(void)mapTypeTapAction:(UITapGestureRecognizer *)tap
{
    if (_mapType == YES) {
        [UIView animateWithDuration:0.5 animations:^{
         self.mapTypeView.frame = CGRectMake(20 * WIDTH, 130 * HEIGHT, SCREEN_WIDTH - 30 * WIDTH, 300);
        }];
        _mapTypeImageView.image = [UIImage imageNamed:@"cuo.png"];
        _mapTypeImageView.backgroundColor = [UIColor whiteColor];
        self.mapType = NO;
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            self.mapTypeView.frame = CGRectMake(SCREEN_WIDTH - 40 * WIDTH, 130 * HEIGHT, 30 * WIDTH, 0);
        }];
        _mapTypeImageView.image = [UIImage imageNamed:@"qiehuandituleixing.png"];
        _mapTypeImageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.246];
        self.mapType = YES;
    }
    
    
    
}
#pragma mark 搜索
-(void)createSearchView
{
    
    self.searchView = [[UIView alloc] initWithFrame:CGRectMake(20 * WIDTH, 20 * HEIGHT, SCREEN_WIDTH - 40 * WIDTH, 44 * HEIGHT)];
    _searchView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.563];
    [self.view addSubview:_searchView];
    
    
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(45 * WIDTH, 0, _searchView.frame.size.width - 90 * WIDTH, 44 * HEIGHT)];
    _searchTextField.placeholder = @" 🔍 查找地点、公交、地铁";
    _searchTextField.delegate = self;
    _searchTextField.backgroundColor = [UIColor clearColor];
    _searchTextField.returnKeyType = UIReturnKeySearch;
    [_searchView addSubview:_searchTextField];
    
    
    
    //返回按钮
    UIImageView *popImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cuo.png"]];
    popImageView.backgroundColor = [UIColor clearColor];
    [_searchView addSubview:popImageView];
    popImageView.userInteractionEnabled = YES;
    [popImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * HEIGHT);
        make.left.mas_equalTo(7 * WIDTH);
        make.width.mas_equalTo(WIDTH * 30);
        make.height.mas_equalTo(HEIGHT * 30);
    }];
    UITapGestureRecognizer *popAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popAction:)];
    [popImageView addGestureRecognizer:popAction];
    
    
    //语音输入
    UIImageView *speechImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yuyin.png"]];
    speechImageView.backgroundColor = [UIColor clearColor];
    [_searchView addSubview:speechImageView];
    speechImageView.userInteractionEnabled = YES;
    [speechImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * HEIGHT);
        make.right.mas_equalTo(- 7 * WIDTH);
        make.width.mas_equalTo(WIDTH * 30);
        make.height.mas_equalTo(HEIGHT * 30);
    }];
    UITapGestureRecognizer *speechAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(speechAction:)];
    [speechImageView addGestureRecognizer:speechAction];
    
    //搜索tableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64 * HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 65;
    [_tableView registerClass:[WYZ_MapTableViewCell class] forCellReuseIdentifier:@"WYZ_MapTableViewCell"];
    _tableView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.631];
    [_mapView addSubview:_tableView];
    
    //初始化检索对象
    _search = [[AMapSearchAPI alloc] init];
    _search.delegate = self;
    
}

#pragma mark 点击return
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_searchTextField resignFirstResponder];
       return YES;
}
#pragma mark 设置标注样式
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        MAPinAnnotationView*annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil)
        {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.canShowCallout= YES;       //设置气泡可以弹出，默认为NO
        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
        annotationView.draggable = YES;        //设置标注可以拖动，默认为NO
        annotationView.pinColor = MAPinAnnotationColorPurple;
        return annotationView;
    }
    return nil;
}
#pragma mark 点击textField触发的方法
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20 * HEIGHT)];
    [_mapView addSubview:statusBarView];
    statusBarView.tag = 100012;
    statusBarView.backgroundColor = [UIColor colorWithRed:0.980 green:1.000 blue:0.975 alpha:0.787];
    [UIView animateWithDuration:0.8 animations:^{
        
        _tableView.frame = CGRectMake(0, 64 * HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64 * HEIGHT);
        _searchView.frame = CGRectMake(0, 20 * HEIGHT, SCREEN_WIDTH, 44 * HEIGHT);
        _searchView.backgroundColor = [UIColor colorWithRed:0.980 green:1.000 blue:0.975 alpha:0.787];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }];
    //监听每次输入调用
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldValue:) name:UITextFieldTextDidChangeNotification object:_searchTextField];
}
-(void)textFieldValue:(NSNotification *)notification
{
    if ([_searchTextField.text isEqualToString:@""]) {
        [self.dataList removeAllObjects];
        [_tableView reloadData];
    }else{
        _searchTextField.clearsOnBeginEditing = NO;
        //构造AMapPOIAroundSearchRequest对象，设置周边请求参数
        AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
        request.location = [AMapGeoPoint locationWithLatitude:_userLocation.coordinate.latitude longitude:_userLocation.coordinate.longitude];
        request.keywords = _searchTextField.text;
        request.types = @"汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施";
        request.sortrule = 0;
        request.offset = 50;
        request.requireExtension = YES;
        request.radius = 5000;
        //发起周边搜索
        [_search AMapPOIAroundSearch: request];
    }
}
#pragma mark 获取当前坐标
-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation
updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        self.userLocation = userLocation;
    }
}
#pragma mark 实现POI搜索对应的回调函数
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if(response.pois.count == 0)
    {
        return;
    }
    self.dataList = [NSMutableArray arrayWithArray:response.pois];
    [_tableView reloadData];
}
#pragma mark tableView协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%ld",_dataList.count);
    return _dataList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WYZ_MapTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WYZ_MapTableViewCell"];
    cell.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.500];
    AMapPOI *poi = _dataList[indexPath.row];
    cell.distanceLabel.text = [NSString stringWithFormat:@"%ldm",poi.distance];
    cell.nameLabel.text = poi.name;
    cell.addressLabel.text = poi.address;
    return cell;
}
#pragma mark tableView点击方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //回地图
    UIView *statusBarView = [self.view viewWithTag:100012];
    [statusBarView removeFromSuperview];
    [UIView animateWithDuration:0.5 animations:^{
        _tableView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT);
        _searchView.frame = CGRectMake(20 * WIDTH, 20 * HEIGHT, SCREEN_WIDTH - 40 * WIDTH, 44 * HEIGHT);
        _searchView.backgroundColor = [UIColor colorWithRed:0.980 green:1.000 blue:0.975 alpha:0.787];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }];
    _searchTextField.clearsOnBeginEditing = YES;
    [_searchTextField endEditing:NO];
    //加云图
    AMapPOI *poi = _dataList[indexPath.row];
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(poi.location.latitude, poi.location.longitude);
    pointAnnotation.title = poi.name;
    pointAnnotation.subtitle = poi.address;
    [_mapView addAnnotation:pointAnnotation];
}
#pragma mark tableView滑动键盘回弹
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_searchTextField resignFirstResponder];
}
#pragma mark 语音输入搜索
-(void)speechAction:(UITapGestureRecognizer *)tap{
    NSLog(@"点击了语音按钮");
}
#pragma mark 返回手势
-(void)popAction:(UITapGestureRecognizer *)tap{
    if (_tableView.frame.origin.y == 64 * HEIGHT) {
        UIView *statusBarView = [self.view viewWithTag:100012];
        [statusBarView removeFromSuperview];
        [UIView animateWithDuration:0.5 animations:^{
            _tableView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT);
            _searchView.frame = CGRectMake(20 * WIDTH, 20 * HEIGHT, SCREEN_WIDTH - 40 * WIDTH, 44 * HEIGHT);
            _searchView.backgroundColor = [UIColor colorWithRed:0.980 green:1.000 blue:0.975 alpha:0.787];
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        }];
        _searchTextField.clearsOnBeginEditing = YES;
        [_searchTextField endEditing:NO];
    }else{
    [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark 实现输入提示的回调函数
-(void)onInputTipsSearchDone:(AMapInputTipsSearchRequest*)request response:(AMapInputTipsSearchResponse *)response
{
    if(response.tips.count == 0)
    {
        return;
    }
    self.dataList = [NSMutableArray array];
    self.dataList = [NSMutableArray arrayWithArray:response.tips];
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //push的时候隐藏navigation
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBar.translucent = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
 
}


@end
