//
//  WYZ_MapTableViewCell.h
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseTableViewCell.h"

@interface WYZ_MapTableViewCell : WYZ_BaseTableViewCell
@property (nonatomic,retain) UILabel *addressLabel;
@property (nonatomic,retain) UILabel *distanceLabel;
@property (nonatomic,retain) UIImageView *addressImageView;
@property (nonatomic,retain) UILabel *nameLabel;
@end
