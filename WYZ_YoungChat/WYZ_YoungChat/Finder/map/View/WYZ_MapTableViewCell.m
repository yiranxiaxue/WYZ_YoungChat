//
//  WYZ_MapTableViewCell.m
//  WYZ_YoungChat
//
//  Created by dllo on 16/3/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_MapTableViewCell.h"

@implementation WYZ_MapTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self creatTabelView];
    }
    
    return self;
}
-(void)creatTabelView
{

    self.addressImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchdingwei.png"]];
    [self.contentView addSubview:_addressImageView];
   
    self.nameLabel = [[UILabel alloc] init];
    _nameLabel.font = [UIFont systemFontOfSize:18];
    [self.contentView addSubview:_nameLabel];
    
    self.addressLabel = [[UILabel alloc] init];
    [self.contentView addSubview:_addressLabel];
    
    self.distanceLabel = [[UILabel alloc] init];
    _distanceLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_distanceLabel];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [_addressImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15 * HEIGHT);
        make.left.mas_equalTo(15 * WIDTH);
        make.width.mas_equalTo(40 * WIDTH);
        make.height.mas_equalTo(40 * HEIGHT);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7 * HEIGHT);
        make.left.mas_equalTo(_addressImageView.mas_right).offset(5 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(20 * HEIGHT);
    }];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_nameLabel.mas_bottom).offset(2 * HEIGHT);
        make.left.mas_equalTo(_addressImageView.mas_right).offset(5 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(18 * HEIGHT);
    }];
    [_distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_addressLabel.mas_bottom).offset(2 * HEIGHT);
        make.left.mas_equalTo(_addressImageView.mas_right).offset(5 * WIDTH);
        make.width.mas_equalTo(300 * WIDTH);
        make.height.mas_equalTo(16 * HEIGHT);
    }];
}
@end
