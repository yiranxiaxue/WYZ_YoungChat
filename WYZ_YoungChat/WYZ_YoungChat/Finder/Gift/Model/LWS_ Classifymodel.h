//
//  LWS_ Classifymodel.h
//  GiftYY
//
//  Created by dllo on 16/3/10.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LWS__Classifymodel : NSObject
@property(nonatomic, copy)NSString *icon_url;
@property(nonatomic, copy)NSString *name;
@property(nonatomic, retain)NSMutableArray *channels;
@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *likes_count;//喜欢次数
@property(nonatomic, copy)NSString *cover_image_url;//图片
@property(nonatomic, copy)NSString *url;//web
@property(nonatomic, retain)NSMutableArray *items;
@property(nonatomic, retain)NSString *appId;
@property(nonatomic, retain)NSMutableArray *subcategories;//礼物数组
@property(nonatomic, copy)NSString *price;
@property(nonatomic, retain)NSNumber *favorites_count;
@property(nonatomic, retain)NSString *content_url;
@end
