//
//  LWS_ClassifyCollectionViewCell.h
//  GiftYY
//
//  Created by dllo on 16/3/10.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWS_ClassifyCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *backImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
