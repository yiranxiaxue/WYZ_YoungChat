//
//  LWS_ItemTableViewCell.h
//  Lws___
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWS_ItemTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *backImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *likeLabel;
- (IBAction)loveButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *loveButton;
@property(nonatomic, assign)BOOL isSelect;
@end
