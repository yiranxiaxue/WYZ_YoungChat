//
//  LWS_CollectionReusableView.h
//  Lws___
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWS_CollectionReusableView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
