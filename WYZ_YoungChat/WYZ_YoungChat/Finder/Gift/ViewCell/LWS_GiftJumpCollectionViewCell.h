//
//  LWS_GiftJumpCollectionViewCell.h
//  Lws___
//
//  Created by dllo on 16/3/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWS_GiftJumpCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *heardImageView;
@property (strong, nonatomic) IBOutlet UILabel *createLabel;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UILabel *downLeftLabel;
@property (nonatomic, assign)BOOL isSelect;
- (IBAction)loveButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *loveButton;

@property (strong, nonatomic) IBOutlet UILabel *downRightLabel;

@end
