//
//  LWS_SelectTitleCollectionViewCell.h
//  Lws___
//
//  Created by dllo on 16/3/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWS_SelectTitleCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
