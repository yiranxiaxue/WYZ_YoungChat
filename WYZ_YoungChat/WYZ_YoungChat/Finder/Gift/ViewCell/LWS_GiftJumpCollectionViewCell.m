//
//  LWS_GiftJumpCollectionViewCell.m
//  Lws___
//
//  Created by dllo on 16/3/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_GiftJumpCollectionViewCell.h"

@implementation LWS_GiftJumpCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.isSelect = NO;
}

- (IBAction)loveButton:(id)sender {
    

    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.values = @[@(0.5),@(1),@(1.5)];
    animation.duration = 0.2;
    [self.loveButton.layer addAnimation:animation forKey:@"animation"];
    if (self.isSelect == YES) {
        [self.loveButton setImage:[UIImage imageNamed:@"iconfont-favoriteoutline.png"] forState:UIControlStateNormal];
    }else{
    
        [self.loveButton setImage:[UIImage imageNamed:@"iconfont-favorite.png"] forState:UIControlStateNormal];
    
    }
    self.isSelect = !self.isSelect;
}
@end
