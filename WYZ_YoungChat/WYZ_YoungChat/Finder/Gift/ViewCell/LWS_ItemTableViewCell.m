//
//  LWS_ItemTableViewCell.m
//  Lws___
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_ItemTableViewCell.h"

@implementation LWS_ItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    self.isSelect = NO;
}

- (IBAction)loveButton:(id)sender {
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.values = @[@(0.5),@(1),@(1.5)];
    animation.duration = 0.2;
    [self.loveButton.layer addAnimation:animation forKey:@"animation"];
    if (self.isSelect == YES) {
        [self.loveButton setImage:[UIImage imageNamed:@"favoriteoutline.png"] forState:UIControlStateNormal];
        
    }else{
    
        [self.loveButton setImage:[UIImage imageNamed:@"iconfont-favorite.png"] forState:UIControlStateNormal];
    
    }
    self.isSelect = !self.isSelect;
}
@end
