//
//  LWS_GiftJumpViewController.m
//  Lws___
//
//  Created by dllo on 16/3/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_GiftJumpViewController.h"
#import "LWS_GiftJumpCollectionViewCell.h"
#import "LWS_JumpViewController.h"
#import "LWS_ Classifymodel.h"
@interface LWS_GiftJumpViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *giftFlowLayout;
@property (strong, nonatomic) IBOutlet UICollectionView *giftJumpCollectionView;
@property(nonatomic, retain)NSMutableArray *itemsArr;
@property(nonatomic, assign)NSInteger count;
@end

@implementation LWS_GiftJumpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
[self createNetWork];
    
    self.title = self.titleName;
    
    
    [self.giftJumpCollectionView setCollectionViewLayout:self.giftFlowLayout];
    [self.giftJumpCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([LWS_GiftJumpCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LWS_GiftJumpCollectionViewCell class])];

    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-fanhui.png"] style:1 target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.giftJumpCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    
    self.giftJumpCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefreshing)];
    
   
    
    
}

-(void)back{

    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"seg" object:nil];

}


-(void)createNetWork{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/item_subcategories/%@/items?limit=20&offset=0",self.appID] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *dataArr = responseObject[@"data"][@"items"];
        self.itemsArr = [NSMutableArray array];
        for (NSDictionary *dic  in dataArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.itemsArr addObject:model];
           
        }
        [self.giftJumpCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];

}

#pragma mark 数据加载
-(void)footerRefreshing{

    self.count += 20;
    [self createData];

    [self.giftJumpCollectionView.mj_footer endRefreshing];


}


-(void)createData{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/item_subcategories/%@/items?limit=20&offset=%ld",self.appID,self.count] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *dataArr = responseObject[@"data"][@"items"];
       
        for (NSDictionary *dic  in dataArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.itemsArr addObject:model];
            
        }
        [self.giftJumpCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];

}

#pragma mark 刷新
-(void)headerRefreshing{

    self.count = 0;
    [self createNetWork];

    [self.giftJumpCollectionView.mj_header endRefreshing];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.itemsArr.count;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    LWS_GiftJumpCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LWS_GiftJumpCollectionViewCell class]) forIndexPath:indexPath];
    LWS__Classifymodel *model = self.itemsArr[indexPath.row];
    [cell.heardImageView setImageWithURL:[NSURL URLWithString:model.cover_image_url]];
    cell.createLabel.text = model.name;
    cell.downLeftLabel.text = model.price;
    cell.downRightLabel.text = [NSString stringWithFormat:@"%@",model.favorites_count];
    return cell;

}

#pragma mark 跳转web

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    LWS_JumpViewController *jump = [[LWS_JumpViewController alloc] initWithNibName:NSStringFromClass([LWS_JumpViewController class]) bundle:nil];
    LWS__Classifymodel *model = self.itemsArr[indexPath.row];
    jump.dataJump = model.url;
    [self.navigationController pushViewController:jump animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
