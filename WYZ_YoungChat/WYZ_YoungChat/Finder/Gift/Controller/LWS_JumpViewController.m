//
//  LWS_JumpViewController.m
//  Lws___
//
//  Created by dllo on 16/3/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_JumpViewController.h"
@interface LWS_JumpViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;


@end

@implementation LWS_JumpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"攻略详情";
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.dataJump]];
    [self.webView loadRequest:request];
self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-fanhui.png"] style:1 target:self action:@selector(web)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    

}

-(void)web{

    [self.navigationController popViewControllerAnimated:YES];

}

-(void)viewWillAppear:(BOOL)animated{
    
    
    self.tabBarController.tabBar.hidden = NO;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
