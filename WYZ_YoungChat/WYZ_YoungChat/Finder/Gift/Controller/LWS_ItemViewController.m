//
//  LWS_ItemViewController.m
//  Lws___
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_ItemViewController.h"
#import "LWS_ItemTableViewCell.h"
#import "LWS_ Classifymodel.h"
#import "LWS_JumpViewController.h"
@interface LWS_ItemViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *itemTableView;
@property(nonatomic, assign)NSInteger count;
@property(nonatomic, retain)NSMutableArray *itemArr;
@property(nonatomic, retain)NSDictionary *jumpWeb;//跳转web
@property(nonatomic, retain)UILabel *title_Label;
@end

@implementation LWS_ItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.title = self.titleName;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-fanhui.png"] style:1 target:self action:@selector(leftApp)];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];

    [self.itemTableView registerNib:[UINib nibWithNibName:NSStringFromClass([LWS_ItemTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LWS_ItemTableViewCell class])];
    
    [self createNetWork];

    [self footerRefreshing];

    self.itemTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    self.itemTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefreshing)];
    
}
-(void)leftApp{

    [self.navigationController popViewControllerAnimated:YES];
    //写一个通知中心
    [[NSNotificationCenter defaultCenter] postNotificationName:@"seg" object:nil];

}

#pragma mark 获取网络数据
-(void)createNetWork{

   
       AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/channels/%@/items?limit=20&offset=%ld",self.backID,self.count]
      parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dataDic = responseObject[@"data"];
        
        NSMutableArray *dataArr = dataDic[@"items"];
        self.itemArr = [NSMutableArray array];
        for (NSDictionary *temp in dataArr){
            LWS__Classifymodel *classModel = [[LWS__Classifymodel alloc] init];
            [classModel setValuesForKeysWithDictionary:temp];
            [self.itemArr addObject:classModel];
        }
      
        
        [self.itemTableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];

   
    AFHTTPSessionManager *managerApp = [AFHTTPSessionManager manager];
    [managerApp GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/collections/%@/posts?gender=1&generation=1&limit=20&offset=0",self.appID]
      parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
  
          self.title_Label.text = responseObject[@"data"][@"title"];
          
          NSArray *arr= responseObject[@"data"][@"posts"];
          
          self.itemArr = [NSMutableArray array];
          
          for (NSDictionary *dic in arr) {
              LWS__Classifymodel *model = [[LWS__Classifymodel alloc]init];
              
              [model setValuesForKeysWithDictionary:dic];
              
              [self.itemArr addObject:model];

          }
   
          [self.itemTableView reloadData];
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      }];
}

//上拉加载
-(void)footerRefreshing{

    self.count += 20;
    [self createLoadData];
    [self.itemTableView.mj_footer endRefreshing];

}

-(void)createLoadData{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/channels/%@/items?limit=20&offset=%ld",self.backID,self.count]
      parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSDictionary *dataDic = responseObject[@"data"];
          
          NSMutableArray *dataArr = dataDic[@"items"];
        for (NSDictionary *temp in dataArr){
              LWS__Classifymodel *classModel = [[LWS__Classifymodel alloc] init];
              [classModel setValuesForKeysWithDictionary:temp];
              [self.itemArr addObject:classModel];
          }
          
          
          [self.itemTableView reloadData];
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      }];
}

#pragma mark 下拉刷新
-(void)headerRefreshing{

    self.count = 0;
    [self createNetWork];
    [self.itemTableView.mj_header endRefreshing];

}

#pragma mark tableView次数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.itemArr.count;


}

#pragma mark tableView重用
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    LWS_ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LWS_ItemTableViewCell class]) forIndexPath:indexPath];
    LWS__Classifymodel *model = self.itemArr[indexPath.row];
    
    cell.titleLabel.text = model.title;
    [cell.backImageView setImageWithURL:[NSURL URLWithString:model.cover_image_url]];
    cell.likeLabel.text = [NSString stringWithFormat:@"%@",model.likes_count];
    
    return cell;


}

#pragma mark 跳转
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    LWS_JumpViewController *jump = [[LWS_JumpViewController alloc] initWithNibName:NSStringFromClass([LWS_JumpViewController class]) bundle:nil];
    LWS__Classifymodel *model = self.itemArr[indexPath.row];
    jump.dataJump = model.content_url;
    [self.navigationController pushViewController:jump animated:YES];
    
}


#pragma mark 不隐藏tabbar
-(void)viewWillAppear:(BOOL)animated{

    self.tabBarController.tabBar.hidden = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
