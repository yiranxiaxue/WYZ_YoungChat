//
//  LWS_ItemViewController.h
//  Lws___
//
//  Created by dllo on 16/3/11.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "WYZ_BaseViewController.h"

@interface LWS_ItemViewController : WYZ_BaseViewController
@property(nonatomic, copy)NSString *backID;
@property(nonatomic, copy)NSString *appID;
@property(nonatomic, copy)NSString *allID;
@property(nonatomic, copy)NSString *titleName;
@end
