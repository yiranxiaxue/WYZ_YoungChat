//
//  LWS_GiftHxmIViewController.m
//  Lws___
//
//  Created by dllo on 16/3/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LWS_GiftHxmIViewController.h"
#import "LWS_ Classifymodel.h"
#import "LWS_SelectTitleCollectionViewCell.h"
#import "LWS_GiftJumpCollectionViewCell.h"
#import "LWS_JumpViewController.h"
#import "LWS_SearchViewController.h"
@interface LWS_GiftHxmIViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *heardCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *heardFlowLayout;
@property (nonatomic, retain)NSMutableArray *filtersArr;
@property (strong, nonatomic) IBOutlet UICollectionView *downCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *downFlowLayout;
@property (nonatomic ,retain) NSMutableArray *downArr;
@property (strong, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) IBOutlet UICollectionView *jumpCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *JumpFlowLayout;

@property (nonatomic, assign)NSInteger isStar;//判断隐藏
@property (nonatomic, retain)NSMutableArray *jumpArr;

@property (nonatomic, assign)NSInteger count;
@property (nonatomic, retain)NSNumber *personality;
@property (nonatomic, retain)NSNumber *scene;
@property (nonatomic, retain)NSString *price;
@property (nonatomic, retain)NSNumber *target;
@property (nonatomic, retain)NSDictionary *dataDic;
- (IBAction)returnButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *returnButton;


@end

@implementation LWS_GiftHxmIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:0.445 blue:0.525 alpha:1.000];
    self.title = @"天女散花";
    
    
    self.isStar = 520;

    self.titleView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.4];
    //标题collectionView
    [self.heardCollectionView setCollectionViewLayout:self.heardFlowLayout];
    self.heardCollectionView.backgroundColor = [UIColor whiteColor];
    [self.heardCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([LWS_SelectTitleCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LWS_SelectTitleCollectionViewCell class])];
    
    //下面collectionView
    [self.downCollectionView setCollectionViewLayout:self.downFlowLayout];
    [self.downCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([LWS_GiftJumpCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LWS_GiftJumpCollectionViewCell class])];
    self.downCollectionView.backgroundColor = [UIColor colorWithWhite:0.932 alpha:1.000];
    //弹出的collectionView
    [self.jumpCollectionView setCollectionViewLayout:self.JumpFlowLayout];
    [self.jumpCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([LWS_SelectTitleCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LWS_SelectTitleCollectionViewCell class])];
    self.jumpCollectionView.backgroundColor = [UIColor whiteColor];
    
    //返回按钮
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fanhui.png"] style:1 target:self action:@selector(returnApp)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    self.returnButton.layer.borderWidth = 1;
    
    [self createNetWork];
    
    [self createJumpNetWork];

    
    self.downCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(footerReferen)];
    
    self.downCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(heardRefenren)];
    
   
    self.titleView.hidden = YES;
    self.jumpCollectionView.hidden = YES;
    
}

#pragma mark 返回按钮方法
-(void)returnApp{

    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"seg" object:nil];
    

}

#pragma mark 获取网络数据
-(void)createNetWork{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://api.liwushuo.com/v2/search/item_filter" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *dataArr = responseObject[@"data"][@"filters"];
        self.filtersArr = [NSMutableArray array];
        for (NSDictionary *temp in dataArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.filtersArr addObject:model];
      
        }
        [self.heardCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

    //downCollectionView 数据
    AFHTTPSessionManager *downManager = [AFHTTPSessionManager manager];
    [downManager GET:@"http://api.liwushuo.com/v2/search/item_by_type?limit=20&offset=0" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *itemsArr = responseObject[@"data"][@"items"];
        self.downArr = [NSMutableArray array];
        for (NSDictionary *temp in itemsArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.downArr addObject:model];
            
            
        }
        [self.downCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

    
    //搜索页面传值刷新
    AFHTTPSessionManager *fromManager = [AFHTTPSessionManager manager];
    [fromManager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/search/item?keyword=%@&limit=20&offset=0&sort=",self.from] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *itemsArr = responseObject[@"data"][@"items"];
        self.downArr = [NSMutableArray array];
        for (NSDictionary *temp in itemsArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.downArr addObject:model];
            
        }
        [self.downCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
//    if ([self.from isEqualToString:@""]) {
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    }

    
}

#pragma mark 创建点击刷新页面数据
-(void)createJumpNetWork{
    AFHTTPSessionManager *jumpManager = [AFHTTPSessionManager manager];
    [jumpManager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/search/item_by_type?limit=20&offset=%ld&personality=%@&price=%@&scene=%@&target=%@",self.count,self.personality,self.price,self.scene,self.target] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *itemsArr = responseObject[@"data"][@"items"];
        self.downArr = [NSMutableArray array];
        for (NSDictionary *temp in itemsArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.downArr addObject:model];
            
            
        }
        [self.jumpCollectionView reloadData];
        [self.downCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}

#pragma mark 标题collectionView 个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.heardCollectionView) {
        return self.filtersArr.count;
    }else if (collectionView == self.downCollectionView){
    
        return self.downArr.count;
    
    }else{
    
        return self.jumpArr.count;
    
    }
}
#pragma mark 标题collectionView 重用标识
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == self.heardCollectionView) {
        LWS_SelectTitleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LWS_SelectTitleCollectionViewCell class]) forIndexPath:indexPath];
        LWS__Classifymodel *model = self.filtersArr[indexPath.row];
        cell.titleLabel.text = model.name;
        cell.titleLabel.textAlignment = 1;
        
        
        return cell;
    }else if(collectionView == self.downCollectionView){
    
        LWS_GiftJumpCollectionViewCell *down = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LWS_GiftJumpCollectionViewCell class]) forIndexPath:indexPath];
       LWS__Classifymodel *model = self.downArr[indexPath.row];
        [down.heardImageView setImageWithURL:[NSURL URLWithString:model.cover_image_url]];
        down.createLabel.text = model.name;
        down.downLeftLabel.text = model.price;
        down.downRightLabel.text = [NSString stringWithFormat:@"%@",model.favorites_count];
        down.backgroundColor = [UIColor whiteColor];

        return down;
    
    }else{

        LWS_SelectTitleCollectionViewCell *jump = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LWS_SelectTitleCollectionViewCell class]) forIndexPath:indexPath];
        jump.titleLabel.text = self.jumpArr[indexPath.row][@"name"];
        jump.titleLabel.textAlignment = 1;
        jump.layer.borderWidth = 1;
        jump.layer.cornerRadius = 5;

        return jump;
    }
}

#pragma mark 加载页面
-(void)lodingData{

    AFHTTPSessionManager *jumpManager = [AFHTTPSessionManager manager];
    [jumpManager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/search/item_by_type?limit=20&offset=%ld&personality=%@&price=%@&scene=%@&target=%@",self.count,self.personality,self.price,self.scene,self.target] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *itemsArr = responseObject[@"data"][@"items"];
        
        for (NSDictionary *temp in itemsArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.downArr addObject:model];
            
            
        }
        [self.jumpCollectionView reloadData];
        [self.downCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];


}
#pragma mark 刷新页面
-(void)renovateData{
    
    AFHTTPSessionManager *jumpManager = [AFHTTPSessionManager manager];
    [jumpManager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/search/item_by_type?limit=20&offset=%ld&personality=%@&price=%@&scene=%@&target=%@",self.count,self.personality,self.price,self.scene,self.target] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *itemsArr = responseObject[@"data"][@"items"];
        self.downArr = [NSMutableArray array];
        for (NSDictionary *temp in itemsArr) {
            LWS__Classifymodel *model = [[LWS__Classifymodel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            [self.downArr addObject:model];
            
            
        }
        [self.jumpCollectionView reloadData];
        [self.downCollectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
    
}


-(void)footerReferen{

    self.count += 20;
    [self lodingData];
    [self.downCollectionView.mj_footer endRefreshing];

    
}

-(void)heardRefenren{
    self.count = 0;
    [self renovateData];
    [self.downCollectionView.mj_header endRefreshing];


}
#pragma mark 标题collectionView 跳转

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (collectionView == self.heardCollectionView) {
        if (self.isStar == indexPath.item) {
            self.titleView.hidden = YES;
            self.jumpCollectionView.hidden = YES;
            self.isStar = 520;

            
        }else{
            
            //数组套数组.把数组内容放到数组里
            self.jumpArr = [NSMutableArray arrayWithArray:[self.filtersArr[indexPath.row] channels]];
            
            NSDictionary *dic = [NSDictionary dictionaryWithObject:@"全部" forKey:@"name"];
            
            [self.jumpArr insertObject:dic atIndex:0];
            
            self.isStar = indexPath.item;
            [self.jumpCollectionView reloadData];
            self.titleView.hidden = NO;
            self.jumpCollectionView.hidden = NO;
}
    }else if (collectionView == self.jumpCollectionView){
    
        
            LWS_SelectTitleCollectionViewCell *cell = (LWS_SelectTitleCollectionViewCell *)[self.heardCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.isStar inSection:0]];
            cell.titleLabel.text = self.jumpArr[indexPath.row][@"name"];
            
            self.personality = self.jumpArr[indexPath.row][@"key"];
            self.price = self.jumpArr[indexPath.row][@"key"];
            self.scene = self.jumpArr[indexPath.row][@"key"];
            self.target = self.jumpArr[indexPath.row][@"key"];
            
            
            self.count = 0;
            self.titleView.hidden = YES;
            [self createJumpNetWork];
 
    }else if (collectionView == self.downCollectionView){
    
        NSString *str = [self.downArr[indexPath.row] appId];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:[NSString stringWithFormat:@"http://api.liwushuo.com/v2/items/%@",str] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.dataDic = responseObject[@"data"];
            LWS_JumpViewController *jump = [[LWS_JumpViewController alloc] initWithNibName:NSStringFromClass([LWS_JumpViewController class]) bundle:nil];
            [self.navigationController pushViewController:jump animated:YES];
            jump.dataJump = self.dataDic[@"url"];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
            }
}

-(void)viewWillAppear:(BOOL)animated{

    self.tabBarController.tabBar.hidden = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)returnButton:(id)sender {
    self.titleView.hidden = YES;
    self.jumpCollectionView.hidden = YES;
    
    
    
}
@end
