//
//  BannarCell.h
//  BannarPic
//
//  Created by dllo on 16/2/24.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYZ_CrosstalkModel.h"

@interface BannarCell : UICollectionViewCell
@property(nonatomic,retain)WYZ_CrosstalkModel *listModel;
@end
