//
//  BannarView.h
//  BannarPic
//
//  Created by dllo on 16/2/24.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

///////// (1).声明一套协议
@protocol BannarView <NSObject>
-(void)string:(NSString *)str;
@end

@interface BannarView : UIView


@property(nonatomic , strong)NSMutableArray *picArr;

//////////  (2).设置代理人属性
@property(nonatomic,assign)id<BannarView>deldete;


@end
