//
//  BannarCell.m
//  BannarPic
//
//  Created by dllo on 16/2/24.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "BannarCell.h"

@interface  BannarCell()
@property(nonatomic , strong)UIImageView *myImageView;
@end

@implementation BannarCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.myImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:self.myImageView];
        
    }
    return self;
}

- (void)setListModel:(WYZ_CrosstalkModel *)listModel{
    if (_listModel != listModel)
    {
        _listModel = listModel;
    }
    [self.myImageView setImageWithURL:[NSURL URLWithString:self.listModel.pic]];
//    NSLog(@"%@",self.myImageView);
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    self.myImageView.frame = CGRectMake(0, 0, CELLWIDTH, CELLHEIGHT);
}

@end
