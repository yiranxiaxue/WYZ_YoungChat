//
//  BannarView.m
//  BannarPic
//
//  Created by dllo on 16/2/24.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "BannarView.h"
#import "BannarCell.h"
#import "WYZ_CrosstalkModel.h"
#import "WYZ_CrosstalkViewController.h"
@interface  BannarView()<UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic,strong)UICollectionView *myCollectionView;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,strong)UIPageControl *myPageControl;

@end

@implementation BannarView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc]init];
        flowLayOut.itemSize = CGSizeMake(CELLWIDTH, CELLHEIGHT);
        flowLayOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayOut.minimumLineSpacing = 0;
        
        self.myCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,0, CELLWIDTH, CELLHEIGHT) collectionViewLayout:flowLayOut];
        self.myCollectionView.backgroundColor =[UIColor whiteColor];
        self.myCollectionView.pagingEnabled = YES;
        self.myCollectionView.dataSource = self;
        self.myCollectionView.delegate = self;
        
        [self.myCollectionView registerClass:[BannarCell class] forCellWithReuseIdentifier:@"cell"];
        
        [self addSubview:self.myCollectionView];
        [self addTimer];
      
        self.myPageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0,0, CELLWIDTH/2, 30)];
        _myPageControl.center = CGPointMake(CELLWIDTH/2,CELLHEIGHT-20);
        [self addSubview:_myPageControl];
        
        
    }
    return self;
}
///点击方法
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
     
      ///(3)设置代理人执行得协议方法
    [self.deldete string:[self.picArr[indexPath.row] albumId]];
     
}
///几个分区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 100;
}

///几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.picArr.count;
}
///collectionViewCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BannarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:1];
    cell.listModel = [self.picArr objectAtIndex:indexPath.item];
    return cell;
}
///set 写入方法
- (void)setPicArr:(NSMutableArray *)picArr{
    if (_picArr != picArr) {
        _picArr = picArr;
    }
    [_myCollectionView reloadData];
    _myPageControl.numberOfPages = _picArr.count;
}
///定时器
- (void)addTimer{
    self.timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(scrollNext) userInfo:nil repeats:YES];
    ///问题:定时器是不是一定准确?
    ///因为 重点::::RunLoop导致 当定时器加到scrollview或者是继承于scrollview的控件上的时候,当滑动scrollview的时候,定时器会停止
    ///解决:
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
}
///定时器方法
- (void)scrollNext{
    ///获取当前显示cell的indexpath
    //    NSIndexPath *currentIndex = [[_myCollectionView indexPathsForVisibleItems]lastObject];
    NSIndexPath *currentIndex = [self returnIndex];
    NSInteger item = currentIndex.item + 1;
    NSInteger section = currentIndex.section;
    
    if (item == _picArr.count) {
        item = 0;
        section++;
    }
//    NSLog(@"item,section = %ld,%ld",item,section);
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
    
    _myPageControl.currentPage = item;
    
    [_myCollectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft  animated:YES];
}


///开始拖拽
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self removeTimer];
}
- (void)removeTimer{
    if (self.timer != nil) {
        ///停止定时器
        [self.timer invalidate];
        ///定时器置空
        self.timer = nil;
    }
    
}


///滑动减速
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self addTimer];
}
///停止拖拽
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _myPageControl.currentPage = (NSInteger)(scrollView.contentOffset.x / WIDTH) % (_picArr.count);
}

- (NSIndexPath  *)returnIndex{
    NSIndexPath *currentIndex = [[_myCollectionView indexPathsForVisibleItems] lastObject];
    currentIndex = [NSIndexPath indexPathForItem:currentIndex.item inSection:1];
    [_myCollectionView scrollToItemAtIndexPath:currentIndex atScrollPosition:UICollectionViewScrollPositionLeft  animated:NO];
    return currentIndex;
}


@end
